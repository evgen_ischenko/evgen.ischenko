package networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) throws IOException {

		BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
		String url = "http://localhost:8080/contacts";
		while (true) {
			System.out.println("Enter url:");
			url = read.readLine();
			if (url.equals("exit")) {
				System.exit(0);
			}
			UrlChecker checker = new UrlChecker(url);
			checker.getResults();
		}
		
	}

	
	
}
