package networking;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UrlChecker {
	
	private URL url;
	private List<String> okUrls  = new ArrayList<>(); 
	private List<String> redirecting  = new ArrayList<>();
	private List<String> badUrls  = new ArrayList<>();
	
	public UrlChecker (String url) {
		try {
			this.url = new URL(url);
			checkInitialUrl();
		} catch (MalformedURLException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void checkInitialUrl () {
		try {
			BufferedReader in = new BufferedReader(
					new	InputStreamReader(url.openStream()));
			List<URL> hrefs = new ArrayList<>();
			String str;
			String subStr;
			while((str = in.readLine()) != null) {
				Matcher m = Pattern.compile("<a.*</a>").matcher(str);
				while (m.find()) {
					int start = m.start(); 
					int end = m.end();
					subStr = str.substring(start, end);
					Matcher m1 = Pattern.compile("href=\".*?\"").matcher(subStr);
					m1.find();
					String href = "";
					href = subStr.substring(m1.start()+6, m1.end()-1);
					if (!href.isEmpty()) {
						try {
							hrefs.add(new URL(href));
						} catch (MalformedURLException e) {
//							e.printStackTrace();
						}
					}
				}	
			}
			in.close();
			checkUrls(hrefs);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void checkUrls(List<URL> urls) throws IOException {
		Iterator<URL> iterator = urls.iterator();
		Map<String, List<String>> map;
		while (iterator.hasNext()) {
			URL currentUrl = iterator.next();
			URLConnection conn = currentUrl.openConnection();
			map = conn.getHeaderFields();
			String resp = "";
			try {
				resp = map.get(null).get(0);
				Integer status = Integer.parseInt(resp.split("\\s")[1]);
				if (200 <= status && status <= 299) {
					okUrls.add(currentUrl.toExternalForm() + " : " + status);
				} else if (300 <= status && status <= 399) {
					redirecting.add(currentUrl.toExternalForm() + " : " + status);
				} else if (400 <= status && status <= 599) {
					badUrls.add(currentUrl.toExternalForm() + " : " + status);
				}
			} catch (NullPointerException e) {
				badUrls.add(currentUrl.toExternalForm() + " : no response...");
			}
		}
	}
	
	public void getResults() {
		Iterator<String> iterator = okUrls.iterator();
		if (!iterator.hasNext()) {System.out.println("No working URLs found.");}
		else System.out.println("Working URLs:");
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		iterator = redirecting.iterator();
		if (!iterator.hasNext()) {System.out.println("No URLs that redirect found.");}
		else System.out.println("URLs that redirect:");
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		iterator = badUrls.iterator();
		if (!iterator.hasNext()) {System.out.println("No bad URLs found.");}
		else System.out.println("Bad URLs:");
		while (iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
	
	
}
