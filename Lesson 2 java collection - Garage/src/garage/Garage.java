package garage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Garage {

	private List<Car> garage = new ArrayList<Car>();
	
	public boolean add (String mark, String color, Integer base) {
		if (!mark.isEmpty() && !color.isEmpty() && base != null) {
			Car car = new Car(mark, color, base);
			garage.add(car);
			return true;
		} else {
			return false;
		}
	}
	
	public String[] getCars () {
		int len = garage.size();
		String[] cars = new String[len];
		for (int i=0; i<len; i++) {
			cars[i] = "Mark: " + garage.get(i).getMark() + "; " +
					"Color: " + garage.get(i).getColor() + "; " +
					"Base length: " + garage.get(i).getBase() + "m;";
		}
		return cars;
	} 
	
	public void sortBy (String by) {
		
		switch (by) {
		case "mark": {
			Collections.sort(garage, new Comparator<Car>(){
				  public int compare(Car c1, Car c2) {
					    return c1.getMark().compareToIgnoreCase(c2.getMark());
					  }
					});
			break;}
		case "color": { 
			Collections.sort(garage, new Comparator<Car>(){
				  public int compare(Car c1, Car c2) {
					    return c1.getColor().compareToIgnoreCase(c2.getColor());
					  }
					});
			break;}
		case "base": { 
			Collections.sort(garage, new Comparator<Car>(){
				  public int compare(Car c1, Car c2) {
					  	int c1Base = Integer.parseInt(c1.getBase());
					  	int c2Base = Integer.parseInt(c2.getBase());
					  	return c1Base - c2Base;
					  }
					});
			break;}
		default: { 
			System.out.println("Cars can be sort only by mark, color or base length.");
			break;}
		} 
		
	}
	
}
