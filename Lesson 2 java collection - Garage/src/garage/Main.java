package garage;


public class Main {

	public static void main(String[] args) {

		Garage garage = new Garage ();
		garage.add("Volvo", "Orange", 2);
		garage.add("VolksWagen", "Purple", 3);
		garage.add("Audi", "White", 1);
		garage.add("Toyota", "Green", 4);
		garage.add("Limousine", "Pink", 10);
		garage.add("Audi", "Black", 1);
		
		String[] cars; 
		int len; 
		
		System.out.println("******************** Sort by Mark ********************");
		
		garage.sortBy("mark");
		cars = garage.getCars();
		len = cars.length;
		for (int i = 0; i < len; i++) {
			System.out.println(cars[i]);
		}
		
		System.out.println("\n******************** Sort by Color ********************");
		
		garage.sortBy("color");
		cars = garage.getCars();
		len = cars.length;
		for (int i = 0; i < len; i++) {
			System.out.println(cars[i]);
		}
		
		System.out.println("\n***************** Sort by Base Length ******************");
		
		garage.sortBy("base");
		cars = garage.getCars();
		len = cars.length;
		for (int i = 0; i < len; i++) {
			System.out.println(cars[i]);
		}
	}

}
