package animal;
import java.util.Random;

import animal.Cat;

public class Main {

	public static void main(String[] args) {
		// Lets assume that user entered enough arguments for 2 cats
		Cat cat1 = new Cat();
		cat1.setAge(5);
		Random rand = new Random();
		int[] color = new int[3];
		for (int i=0; i<3; i++) {
			color[i] = rand.nextInt(3);
		}
		cat1.setColor(color);
		Cat cat2 = new Cat();
		cat2.setAge(5);
		for (int i=0; i<3; i++) {
			color[i] = rand.nextInt(3);
		}
		cat2.setColor(color);
		System.out.println("Cat1's age is: " + cat1.getAge());
		System.out.println("Cat2's age is: " + cat2.getAge());
		for (int i=0; i<3; i++) {
			System.out.println("Cat1's color is: " + cat1.getColor()[i]);
		}
		for (int i=0; i<3; i++) {
			System.out.println("Cat2's color is: " + cat2.getColor()[i]);
		}
		if (cat1.equals(cat2)) {
			System.out.println("Cats are equal.");
		} else {
			System.out.println("Cats are not equal.");
		}
	}

}
