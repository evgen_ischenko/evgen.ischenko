package animal;

public class Cat {

	private Integer age;
	private Integer[] color = new Integer[3];
	
	public static String toString (Cat cat) {
		String catStr;
		catStr = cat.age.toString();
		for (int i = 0; i < 3; i++) {
			catStr = catStr + " " + cat.getColor()[i].toString();
		}
		return catStr;
	} 
	
	public boolean equals (Cat cat2) {
		if (this.toString(this).hashCode() == cat2.toString(cat2).hashCode()) {
			return true;
		} else {
			return false;
		}
	}

	public void setColor(int color[]) {
		for (int i=0; i<3; i++) {	
			this.color[i] = color[i];
		}
	}
	
	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer[] getColor() {
		return color;
	}

}
