
public class FibonacciSequence {

	public static void main (String args[]) {
		try {
			long n = Integer.parseInt(args[0]);
			if (n > 1) {
				for (int i=2; i <= n+2; i++) {
					System.out.println("f[" + (i-2) + "] = " +  positiveFibonacci(i)[i-2]);
				}
			} else if (n < -1) {
				for (int i=2; i <= (n*-1)+2; i++) {
					float negFibonacci = (float) (Math.pow(-1, i+1)*positiveFibonacci(i)[i-2]);
					System.out.println("f[" + (i-2) + "] = " +  negFibonacci);
				}
			} else {
				System.out.println("Argument should not be equal -1, 0 or 1.");
			}
		} catch (IllegalArgumentException e) {
			System.out.println("Argument is wrong, should be integer.");
		}
	}
	
	public static long[] positiveFibonacci (int input) {
		long[] f;
		f = new long[input];
		f[0] = 0;
		f[1] = 1;
		for (int i = 2; i < input; i++) {
			f[i] = f[i-1] + f[i-2];
		}
		return f;
	}
	
}
