package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class Main {

	public static void main(String[] args) throws IOException {

		String req;
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			try {
				System.out.println("Type something:");
				Socket socket = new Socket("localhost", 23);
				OutputStream out = socket.getOutputStream();
				InputStream in = socket.getInputStream();
				req = reader.readLine();
				if (req.equals("exit")) { socket.close(); System.exit(0); }
				out.write(req.getBytes());
				byte[] buf = new byte[1];
				in.read(buf);
				System.out.println("Server response: " + new String(buf));
				socket.close();
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
	}

}
