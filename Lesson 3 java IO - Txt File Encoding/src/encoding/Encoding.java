package encoding;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;

public class Encoding {

	public boolean changeEncoding (File file, String code) {
		try {
			String s = getFileContent(file, code);
			String fileName = file.getPath();
			file.delete();
			file = new File(fileName);
				Writer writer = new OutputStreamWriter(
						new FileOutputStream(file), code);
					writer.write(s);
				writer.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Wrong encoding...");
			return false;
		}
		return true;
	}
	
	public String getFileContent (File file, String code) {
		if (!file.canRead() || !file.exists()) {
			System.out.println(file + " not found.");
			return null;
		}
		String s = "";
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		FileInputStream fileInputStream = null;
		Reader reader = null;
		try {
			char[] c = new char[(int) file.length()];
			fileInputStream = new FileInputStream(file);
			reader = new  InputStreamReader(fileInputStream, code);
			reader.read(c);
			s = new String(c);
		} catch (IOException e) {
			// wrong encoding, do nothing
		} finally {
			try {
//				baos.flush();
//				baos.close();
				reader.close();
				if (null != fileInputStream) {
					fileInputStream.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return s;
	}
	
	public void getEncodings () {
		File file = new File("codePages.txt");
		String s = getFileContent(file, "Cp1250");
		System.out.println(s);
	}
	
}
