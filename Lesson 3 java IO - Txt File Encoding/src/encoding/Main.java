package encoding;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) {
		
		try {
			Encoding enc = new Encoding();
			BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
			String s = "";
			String f = "";
			File file = null;
			while (f.isEmpty() || !file.isFile()) {
				if (f.isEmpty()) {
					System.out.println("Please, enter file name and it's encoding");
				} else if (!file.isFile()) {
					System.out.println(f + " doesn't exist or not a file");
				}
				s = bufferRead.readLine();
				f = s.split("\\s")[0];
				file = new File(f);
			}
			String oldCode = "Cp1251"; // default
				try {
					oldCode = s.split("\\s")[1];
					System.out.println("Before:\n" + enc.getFileContent(file, oldCode));
				} catch (ArrayIndexOutOfBoundsException | NullPointerException e) {/* In case user has entered wrong encoding */
					System.out.println("Entered wrong initial encoding, \"Cp1251\" will be used.");
					oldCode = "Cp1251";
					System.out.println("Before:\n" + enc.getFileContent(file, oldCode));
				}
			enc.getEncodings();
			String reason = "";
			while (true) {
				if (!reason.equals("Wrong newEncoding")) {
					System.out.println("Please, choose one of the encodings above");
				} else {
					System.out.println("Wrong encoding, try again...");
				}
				s = bufferRead.readLine();
				try {
					enc.changeEncoding(file, s);
					System.out.println("After:\n" + enc.getFileContent(file, s));
					break;
				} catch (NullPointerException e) {
					reason = "Wrong newEncoding";
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

}
