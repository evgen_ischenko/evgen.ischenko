
public class CharSequence {
	public static void main (String args[]) {
		int startChar = Integer.parseInt(args[0]), endChar = Integer.parseInt(args[1]);
		for (int i = startChar; i <= endChar; i++) {
			char c = (char) i;
			System.out.println("ASCII code: " + i + ". Character: " +  c);
		}
	}
}
