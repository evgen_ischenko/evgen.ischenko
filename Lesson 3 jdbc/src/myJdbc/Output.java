package myJdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Output {
	
	private ResultSet resultSet;
	private int columnCount;
	private Map<String, Integer> map = new LinkedHashMap<String, Integer>();
	private List<String> columns = new ArrayList<String>();

	public Output (ResultSet resultSet) throws SQLException {
		this.resultSet = resultSet;
		columnCount = resultSet.getMetaData().getColumnCount();
		
			try {
				for (int i = 0; i < columnCount; i++) {
					columns.add(resultSet.getMetaData().getColumnName(i+1));
				}
			} catch (NullPointerException e) {
				// in case of nulls in table
			}
		
		getLongestItem();
		print();
	}

	// Looking for max row length in each column
	private void getLongestItem () throws SQLException {
		resultSet.beforeFirst();
		int newLen = 0;
		for (String colName : columns) {
			int len = 0;
			while (resultSet.next()) {
				if (len < colName.length()) {
					len = colName.length();
				}
				try {
					newLen = resultSet.getString(colName).length();
				} catch (NullPointerException e) {
					// in case of nulls in table
				}
				if (len < newLen) {
					len = newLen;
				}
			}
			map.put(colName, len);
			resultSet.beforeFirst();
		}
		resultSet.beforeFirst();
	}
	
	// Printing out formatted table
	public void print () throws SQLException {
		
		String firstRow = "╔";
		for (int i = 0; i < map.size(); i++) {
			for (int j = 0; j < map.get(columns.get(i)); j++) {
				firstRow = firstRow + "═";
			}
			if (map.size() == i+1) {
				firstRow = firstRow + "╗";
			} else {
				firstRow = firstRow + "╦";
			}
		}
		System.out.println(firstRow);
		
		header();
		
		nextRow();
		
		String lastRow = "\n╚";
		for (int i = 0; i < map.size(); i++) {
			for (int j = 0; j < map.get(columns.get(i)); j++) {
				lastRow = lastRow + "═";
			}
			if (map.size() == i+1) {
				lastRow = lastRow + "╝\n";
			} else {
				lastRow = lastRow + "╩";
			}
		}
		System.out.println(lastRow);
		
		resultSet.last();
		
		System.out.println(resultSet.getRow() + " rows fetched");
		
	}
	
	// Query results
	private String nextRow () throws SQLException {
		String nextRow = "";
		resultSet.beforeFirst();
		while (resultSet.next()) {
			for (String colName : columns) {
				nextRow = nextRow + "║" + resultSet.getString(colName);
				
				try {
					for (int j = resultSet.getString(colName).length() ; j < map.get(colName); j++) {
						nextRow = nextRow + " ";
					}
				} catch (NullPointerException e) {
					// in case of nulls in table
					for (int j = 4 ; j < map.get(colName); j++) {
						nextRow = nextRow + " ";
					}
				}
				
			}
			nextRow = nextRow + "║";
			if (resultSet.next()) {
				nextRow = nextRow + separator();
			}
			resultSet.previous();
		}
		resultSet.beforeFirst();
		System.out.print(nextRow);
		return nextRow;
	}
	
	// Row with table header
	private String header() {
		String header = "║";
		for (int i = 0; i < map.size(); i++) {
			header = header + columns.get(i);
			for (int j = columns.get(i).length() ; j < map.get(columns.get(i)); j++) {
				header = header + " ";
			}
			if (map.size() == i+1) {
				header = header + "║";
			} else {
				header = header + "║";
			}
		}
		header = header + separator();
		System.out.print(header);
		return header;
	}
	
	// Separator between rows
	private String separator() {
		String separator = "\n╠";
		for (int i = 0; i < map.size(); i++) {
			for (int j = 0; j < map.get(columns.get(i)); j++) {
				separator = separator + "═";
			}
			if (map.size() == i+1) {
				separator = separator + "╣\n";
			} else {
				separator = separator + "╬";
			}
		}
		return separator;
	}

}
