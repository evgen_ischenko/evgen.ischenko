package myJdbc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

	public static void main(String[] args) {
//		select * from users u left join testTable t on u.entityId = t.column1
//		String url = "jdbc:mysql://localhost/contacts";
		String url = "";
		String user = "";
		String pass = "";
		String s = "";
		
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in)); 
		
		
		while (true) {
			
			try {
				if (url.isEmpty()) {
					System.out.println("Enter url to Database:");
					url = bufferedReader.readLine();
				}
				if (user.isEmpty()) {
					System.out.println("Enter user:");
					user = bufferedReader.readLine();
				}
				if (pass.isEmpty()) {
					System.out.println("Enter password:");
					pass = bufferedReader.readLine();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if (!url.isEmpty() && !user.isEmpty() && !pass.isEmpty()) {
				break;
			}
		}
		
		String sql = null;
		while (true) {
			System.out.println("Enter your query or 'exit' to leave...");
		
			try {
				sql = bufferedReader.readLine();
			} catch (IOException e1) {
				e1.getMessage();
			}
			
			if (sql.equalsIgnoreCase("exit")) {
				System.exit(0);
			}
			
			try {
				DbManager mngr = new DbManager(url, user, pass);
				mngr.getConnection(sql);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
		}
	}

}
