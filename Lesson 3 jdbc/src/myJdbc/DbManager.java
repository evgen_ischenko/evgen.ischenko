package myJdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbManager {
	
	private static String url;
	private static String user;
	private static String pass;
	
	public DbManager (String url, String user, String pass) throws SQLException {
		try {
		    Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		this.url = url;
		this.user = user;
		this.pass = pass;
	}
	
	public void getConnection (String sql) throws SQLException {
		
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String command = sql.split("\\s")[0];
		
		try {
			
			connection = DriverManager.getConnection(url, user, pass);
			statement = connection.createStatement();
			
			if (command.equalsIgnoreCase("SELECT")) {
				resultSet = statement.executeQuery(sql);
				if (resultSet.next()) {
					Output out = new Output(resultSet);
				} else {
					System.out.println("<No data to display>");
				}
				resultSet.beforeFirst();
			} else {
				long startTime = System.currentTimeMillis();
				int affected = statement.executeUpdate(sql);
				long endTime = System.currentTimeMillis();
				if (affected == 1) {
					System.out.println("Query OK, " + affected + " row affected (" + (endTime - startTime) + " ms)");
				} else {
					System.out.println("Query OK, " + affected + " rows affected (" + (endTime - startTime) + " ms)");
				}
			}
						
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
	}
	
}
