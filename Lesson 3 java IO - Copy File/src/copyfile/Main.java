package copyfile;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) throws IOException {
		FileManager manager = new FileManager();
		
		BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));		
		String src = "";
		String dst = "";
		String bfr = "";
		
		while ((src.isEmpty() || dst.isEmpty()) || bfr.isEmpty()) {
			if (src.isEmpty()) {
				System.out.println("Please, enter file which you want to copy:");
				src = bufferRead.readLine();
			}
			if (dst.isEmpty()) {
				System.out.println("Please, specify target file name:");
				dst = bufferRead.readLine();
			}
			if (bfr.isEmpty()) {
				System.out.println("Do you want use buffer? yes or no:");
				bfr = bufferRead.readLine();
				if (!bfr.equalsIgnoreCase("yes") && !bfr.equalsIgnoreCase("no")) {
					System.out.println("Wrong answer...");
					bfr="";
				}
			}
		}
		
		File source = new File(src);
		if (!source.exists()) {
			System.out.println(source + " does not exist");
		} else {
			
			File dest = new File(dst);
			if (dest.exists()) {
				System.out.println(dest + " already exists");
				System.exit(0);
			} 
			
			System.out.println("Please, wait...");
			
			long startTime = System.currentTimeMillis();
			
			manager.copy(source, dest, bfr);
//			manager.copyUsingFiles(source, dest);
			
			long endTime = System.currentTimeMillis();
			
			System.out.println("Operation took " + (endTime - startTime) + " milliseconds");
		}
	}

}
