package copyfile;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

public class FileManager {

	public void copy (File source, File dest ,String s) {
		if (s.equalsIgnoreCase("yes")) {
			copyBuffer(source, dest);
		} else if (s.equalsIgnoreCase("no")) {
			copyWOBuffer(source, dest);
		}
	}
	
	private void copyBuffer (File source, File dest) {
		try {
			BufferedInputStream bis = new BufferedInputStream(new FileInputStream(source));
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(dest));
			byte[] buf = new byte[100];
			int len = 0;
			while ((len = bis.read(buf)) > 0) {
				bos.write(buf, 0, len);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void copyWOBuffer (File source, File dest) {
		FileInputStream fileInputStream = null;
		FileOutputStream fileOutputStream = null;
		try {
			fileInputStream = new FileInputStream(source);
			fileOutputStream = new FileOutputStream(dest);
			byte[] buf = new byte[1024];
			int len = 0;
			while ((len = fileInputStream.read(buf)) > 0) {
				fileOutputStream.write(buf, 0, len);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void copyUsingFiles (File source, File destination) {
		try {
			Files.copy(source.toPath(), destination.toPath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
