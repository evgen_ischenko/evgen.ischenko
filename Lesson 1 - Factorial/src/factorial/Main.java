package factorial;

/**
 * Created with IntelliJ IDEA.
 * User: Evgen
 * Date: 28.04.13
 * Time: 11:44
 * To change this template use File | Settings | File Templates.
 */
public class Main {

	public static void main(String args[]) {
		int toFactorial = 1;
		try {
			toFactorial = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (ArrayIndexOutOfBoundsException e1) {
			e1.printStackTrace();
		} finally {
			int factorial = 1;
			for (int i = 1; i <= toFactorial; i++) {
				factorial *= i;
			}
			System.out.println("Factorial of " + toFactorial + ": " + factorial);
		}
	}

}
