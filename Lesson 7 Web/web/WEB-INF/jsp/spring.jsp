<%@ page import="javax.servlet.http.HttpSession" %>
<%@ page import="java.io.PrintWriter" %>
<%@ page import="java.util.Enumeration" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<title>Lesson 7 Web</title>
	</head>
	<body>
		<div style="width: 100%; height: 100%">
		
		<div style="width: 100%; height: 33%; vertical-align: top;">
			<div align="left" style="width: 33%; visibility: hidden">1</div>
			<div align="center" style="width: 33%; visibility: hidden">2</div>
			<div align="right" style="width: 33%; visibility: hidden">3</div>
		</div>
		
		<div style="vertical-align: middle; height: 33%;">	
			<div style="width: 35%; visibility: hidden; float: left;">1</div>
				<div style="width: 25%; float: left;">
				<c:set var="sess" value="${sessionScope}" />
				<form name="add" action="" method="get">
					<table>
					<thead>
						<tr>
							<td><span>Name</span></td>
							<td><span>Value</span></td>
							<td><span>Action</span></td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="i" items="${attr}">
							<tr>
								<td><span>${i}</span></td>
								<td><span>${sessionScope[i]}</span></td>
								<td><a href="?action=delete&name=${i}">delete</a></td>
							</tr>
						</c:forEach>
						<tr>
							<td>
								<input type="text" style="display: none;" value="add" name="action"/>
								<input type="text" name="name" />
							</td>
							<td><input type="text" name="value" /></td>
							<td>
								<input type="submit" value="Submit" >
							</td>
						</tr>
					</tbody>
					</table>
				</form>
				</div>
			<div style="width: 40%;"></div>
		</div>	
			
		<div style="width: 100%; height: 33%; vertical-align: bottom;">
			<div style="width: 33%;"></div>
			<div style="width: 33%;"></div>
			<div style="width: 33%;"></div>
		</div>
			
			</div>
		</div>
	</body>
</html>