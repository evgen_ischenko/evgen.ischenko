package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ManagerServlet extends HttpServlet {

	private RequestDispatcher dispatch;
	private String name;
	private String value;
	private ServletContext context;
	private HttpSession session = null;
	
	public void init(ServletConfig config) throws ServletException {
		context = config.getServletContext();
		dispatch = context.getRequestDispatcher("/WEB-INF/jsp/home.jsp");
	}
	
	public void doGet (HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		if (null == session) {
			session = request.getSession();
		}
		String act = request.getParameter("action");
		name = request.getParameter("name");
		value = request.getParameter("value");
		request.removeAttribute("sessionNames");
		if (act != null) {
			switch (act) {
			case "add": {
				addAttributeTo();
				request.setAttribute("sessionNames", session.getAttributeNames());
				dispatch.forward(request, response);
				break;
			}
			case "delete": {
				removeAttributeFrom();
				request.setAttribute("sessionNames", session.getAttributeNames());
				dispatch.forward(request, response);
				break;
			}
			default: {
				request.setAttribute("sessionNames", session.getAttributeNames());
				dispatch.forward(request, response);
				break;
			}
			}
		} else {
			dispatch.forward(request, response);
		}
		
	}
	
	public void doPost (HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
	}
	
	
	public void removeAttributeFrom() {
			session.removeAttribute(name);
	}
	
	public void addAttributeTo() {
			session.setAttribute(name, value);
	}

	public void updateAttributeIn() {
			session.setAttribute(name, value);
	}

}
