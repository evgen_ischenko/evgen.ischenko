package servlets;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@org.springframework.stereotype.Controller
public class ControllerClass {

	private HttpSession session;

	@RequestMapping(value="/q.html")
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		if (null == session) {
			session = request.getSession();
		}

		ModelAndView modelAndView = new ModelAndView("spring");

		String name = (String) request.getParameter("name");
		String value = (String) request.getParameter("value");
		
		if (request.getParameter("action") != null) {
			String act = (String) request.getParameter("action");
			switch (act) {
			case "add": {
				session.setAttribute(name, value);
				break;
			}
			case "delete": {
				session.removeAttribute(name);
				break;
			}
			default: {
				break;
			}
			}
		}
		
		modelAndView.addObject("attr", session.getAttributeNames());

		return modelAndView;
	}
}
