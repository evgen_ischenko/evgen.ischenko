package hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class UserManager {

	private Session session;
	
	public List<User> getUsersByGroup(Group group) {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(User.class).add(
				Restrictions.eq("groupId", group.getGroupId()));
		List<User> list = criteria.list();
		session.close();
		return list;
	}

	public User getUserById(Integer id) {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(User.class).add(
				Restrictions.eq("groupId", id));
		User user = (User) criteria.list().get(0);
		session.close();
		return user;
	}

	public List<User> getAllUsers() {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(User.class);
		List<User> list = criteria.list();
//		session.flush();
//		session.clear();
		session.close();
		return list; 
	}

	public boolean addUser(String fName, String lName, String login, String pass, String groupId) {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("INSERT INTO users "
				+ "(first_name, last_name, login, password, groupId) " + "VALUES ('"
				+ fName + "', '" + lName + "', '" + login + "' , '" + pass + "' , '" + groupId
				+ "') ");
		int r = query.executeUpdate();
		session.close();
		if (r != 0) {
			return true;
		}
		return false;
	}

	public boolean updateUserById(Integer userId, String column, String value) {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("UPDATE users SET " + column
				+ " = " + value + " WHERE users.userId = " + userId);
		int r = query.executeUpdate();
		session.close();
		if (r != 0) {
			return true;
		}
		return false;
	}
	
	public boolean updateMultipleFieldsById(Integer userId, String[] column, String[] value) {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		String toUpdate = "";
		int i = 0;
		for (String col : column) {
			toUpdate = toUpdate + " " + col + " = '" + value[i] + "' , ";
			i++;
		}
		toUpdate = toUpdate.substring(0, toUpdate.length()-3);
		SQLQuery query = session.createSQLQuery("UPDATE users SET " + toUpdate + " WHERE users.userId = " + userId);
		int r = query.executeUpdate();
		session.close();
		if (r != 0) {
			return true;
		}
		return false;
	}
	
	public boolean updateUserByLogin(String login, String column, String value) {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("UPDATE users SET " + column
				+ " = " + value + " WHERE users.login = " + login);
		int r = query.executeUpdate();
		session.close();
		if (r != 0) {
			return true;
		}
		return false;
	}

	public boolean deleteUser(String login) {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("DELETE FROM users WHERE login = " + login);
		int r = query.executeUpdate();
		session.close();
		if (r != 0) {
			return true;
		}
		return false;
	}
	
	public boolean deleteUserById(Integer id) {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("DELETE FROM users WHERE userId = " + id);
		int r = query.executeUpdate();
		session.close();
		if (r != 0) {
			return true;
		}
		return false;
	}

}
