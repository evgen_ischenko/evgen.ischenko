package hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class GroupManager {

	private Session session;

	public List<Group> getAllGroups() {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(Group.class);
		List<Group> list = criteria.list();
		session.close();
		return list;
	}

	public Group getGroupById(Integer id) {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		Criteria criteria = session.createCriteria(Group.class).add(
				Restrictions.eq("groupId", id));
		Group group;
		try {
			group = (Group) criteria.list().get(0);
		} catch (IndexOutOfBoundsException e) {
//			session.flush();
//			session.clear();
			session.close();
			return null;
		}
//		session.flush();
//		session.clear();
		session.close();
		return group;
	}

	public void addGroup(String name) {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		SQLQuery sql = session
				.createSQLQuery("INSERT INTO groups (name) VALUES ('" + name
						+ "')");
		sql.executeUpdate();
		session.close();
	}

	public void deleteGroup(String name) {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		SQLQuery sql = session
				.createSQLQuery("DELETE FROM groups WHERE groups.name = '"
						+ name + "'");
		sql.executeUpdate();
		session.close();
	}

	public void deleteGroupById(Integer groupId) {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		SQLQuery sql = session
				.createSQLQuery("DELETE FROM groups WHERE groups.Id = "
						+ groupId);
		sql.executeUpdate();
		session.close();
	}

	public void saveGroup(Group group) {
		session = HibernateUtils.getSessionFactory().openSession();
		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("UPDATE groups SET name = '"
				+ group.getName() + "' WHERE ID = "
				+ group.getGroupId());
		int r = query.executeUpdate();
		session.close();
	}

}
