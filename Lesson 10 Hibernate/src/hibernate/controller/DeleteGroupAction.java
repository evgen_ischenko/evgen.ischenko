package hibernate.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;

public class DeleteGroupAction {

	@RequestMapping(value="/deleteGroupAction.do")
	public void handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		ControllerClass.data.gm.deleteGroupById(Integer.parseInt(request.getParameter("groupId")));
		
		response.sendRedirect("./viewGroupsAction.do");
	}
	
}
