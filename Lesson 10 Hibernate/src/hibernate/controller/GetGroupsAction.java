package hibernate.controller;

import hibernate.Group;
import hibernate.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

public class GetGroupsAction {

	@RequestMapping(value = "/viewGroupsAction.do")
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		ModelAndView modelAndView = new ModelAndView("groupView");

		List<Group> groups = ControllerClass.data.gm.getAllGroups();

		Map<Group, List<User>> usersByGroup = new HashMap<>();

		for (Group group : groups) {
			List<User> users = ControllerClass.data.um.getUsersByGroup(group);
			usersByGroup.put(group, users);
		}

		request.setAttribute("groups", groups);
		request.setAttribute("usersByGroup", usersByGroup);

		return modelAndView;
	}

}
