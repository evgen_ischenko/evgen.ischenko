package hibernate.controller;

import hibernate.Group;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;

public class AddGroupAction {

	@RequestMapping(value="/addGroupAction.do")
	public void handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String groupName = request.getParameter("name");
		
		ControllerClass.data.gm.addGroup(groupName);
		
		response.sendRedirect("./viewGroupsAction.do");
	}
	
	@RequestMapping(value="/updateGroupAction.do")
	public void saveGroup(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		Group group = new Group();
		
		group.setGroupId(Integer.parseInt(request.getParameter("groupId")));
		group.setName(request.getParameter("name"));
		
		ControllerClass.data.gm.saveGroup(group);
		
		response.sendRedirect("./viewGroupsAction.do");
	}

}
