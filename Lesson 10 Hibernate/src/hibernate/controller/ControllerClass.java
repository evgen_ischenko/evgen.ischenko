package hibernate.controller;

import hibernate.DataManager;
import hibernate.Group;
import hibernate.HibernateUtils;
import hibernate.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

public class ControllerClass {
	
	public static DataManager data = new DataManager();
	public static HibernateUtils utils = new HibernateUtils();
	
	@RequestMapping(value="/homeAction.do")
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelAndView modelAndView = new ModelAndView("hibernate");

		List<User> users = new ArrayList<>(); 
		users = data.um.getAllUsers();
		
		Map<User, Group> map = new HashMap<>();
		
		for (User user : users) {
			Group group = data.gm.getGroupById(user.getGroupId());
			map.put(user, group);
		}
		
		request.setAttribute("groupUserMap", map);
		request.setAttribute("users", users);
		
//		if (null != request.getParameter("userId")) {
			List<Group> groups = data.gm.getAllGroups();
			request.setAttribute("groups", groups);
//		}
		
		return modelAndView;
	}
	
}
