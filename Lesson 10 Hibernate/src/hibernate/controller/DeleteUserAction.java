package hibernate.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;

public class DeleteUserAction {

	@RequestMapping(value = "/deleteUserAction.do")
	public void handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		if (null != request.getParameter("groupId")) {
			ControllerClass.data.um.updateUserById(
					Integer.parseInt(request.getParameter("userId")),
					"groupId", "0");
			response.sendRedirect("./viewUsersByGroup.do?groupId=" + Integer.parseInt(request.getParameter("groupId")));
		} else {
			ControllerClass.data.um.deleteUserById(Integer.parseInt(request
					.getParameter("userId")));
			response.sendRedirect("./homeAction.do");
		}
	}

}
