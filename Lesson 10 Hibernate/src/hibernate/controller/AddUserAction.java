package hibernate.controller;

import hibernate.DataManager;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;

import antlr.StringUtils;

import sun.io.ByteToCharASCII;
import sun.io.ByteToCharUTF8;
import sun.io.ByteToCharUnicode;

public class AddUserAction {

	@RequestMapping(value = "/addUserAction.do")
	public void handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		if (null != request.getParameter("groupId")) {
			ControllerClass.data.um.updateUserById(
					Integer.parseInt(request.getParameter("userId")),
					"groupId", request.getParameter("groupId"));
			response.sendRedirect("./viewUsersByGroup.do?groupId=" + Integer.parseInt(request.getParameter("groupId")));
		} else {
			Map<String, String> map = new HashMap<>();
			Enumeration<String> enums = request.getParameterNames();
			while (enums.hasMoreElements()) {
				String paramName = enums.nextElement();
				map.put(paramName, request.getParameter(paramName));
			}

			// TODO make random password generation

			ControllerClass.data.um.addUser(map.get("fName"), map.get("lName"),
					map.get("login"), "password", map.get("groupIdNewUser"));

			response.sendRedirect("./homeAction.do");
		}
	}

}
