package hibernate.controller;

import hibernate.Group;
import hibernate.User;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

public class UsersByGroupAction {

	@RequestMapping(value="/viewUsersByGroup.do")
	public ModelAndView handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelAndView modelAndView = new ModelAndView("usersByGroup");

		Integer id = Integer.parseInt(request.getParameter("groupId"));
		
		Group g = ControllerClass.data.gm.getGroupById(id);
		
		List<User> users = ControllerClass.data.um.getUsersByGroup(g);
		
		request.setAttribute("group", g);
		request.setAttribute("users", users);
		
		return modelAndView;
	}
	
}
