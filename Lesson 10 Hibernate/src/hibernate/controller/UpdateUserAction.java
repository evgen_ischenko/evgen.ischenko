package hibernate.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;

public class UpdateUserAction {
	@RequestMapping(value = "/updateUserAction.do")
	public void handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		String[] column = {"first_name", "last_name", "login", "groupId"};
		String[] value = {"", "", "", ""};
		
		int i = 0;
		for (String col : column) {
			value[i] = request.getParameter(col);
			i++;
		}
		
		ControllerClass.data.um.updateMultipleFieldsById(Integer.parseInt(request.getParameter("userId")), column, value);
		
		response.sendRedirect("./homeAction.do");
	}
}
