<%@ page import="javax.servlet.http.HttpSession"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="java.util.Enumeration"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link rel="icon" type="image/png" href="/hibernate/pic/favicon.ico">
<title>Hibernate</title>
<script type="text/javascript">
	function show(id) {
		document.getElementById(id).style.display = "block";
	}
	function hide(id) {
		document.getElementById(id).style.display = "none";
	}
</script>
</head>
<body>
	<div style="width: 100%; height: 100%">

		<div style="width: 100%; height: 33%; vertical-align: top;">
			<div align="left" style="width: 33%; visibility: hidden">1</div>
			<div align="center" style="width: 33%; visibility: hidden">2</div>
			<div align="right" style="width: 33%; visibility: hidden">3</div>
		</div>
		<div style="vertical-align: middle; height: 33%;">
			<div style="width: 25%; visibility: hidden; float: left;">1</div>
			<div style="width: 35%; float: left;">
				<table style="float: none;" border="2px">
					<tbody>
						<tr align="center" valign="middle">
							<td width="8px" style="padding: 2px;">#</td>
							<td style="padding: 2px;">Group Name</td>
							<td style="padding: 2px;">View Users</td>
							<td style="padding: 2px;">Edit Group</td>
							<td style="padding: 2px;">Delete</td>
						</tr>
						<c:set var="i" value="1" />
						<c:forEach var="group" items="${requestScope.groups}">
							<c:choose>
								<c:when test="${param.groupId != group.groupId}">
									<tr>
										<td width="8px" style="padding: 2px;">${i}</td>
										<td style="padding: 2px;">${group.name}</td>
										<td style="padding: 2px;"><a
											onmouseover="show('groupUsers_${group.groupId}')"
											onmouseout="hide('groupUsers_${group.groupId}')"
											href="./viewUsersByGroup.do?groupId=${group.groupId}">View
												Users</a></td>
										<td style="padding: 2px;"><a
											href="./viewGroupsAction.do?groupId=${group.groupId}">
												Edit Group</a></td>
										<td style="padding: 2px; text-align: center;"><a
											href="./deleteGroupAction.do?groupId=${group.groupId}"><img
												width="35px" alt="Delete"
												src="http://localhost:8080/hibernate/pic/deleteUser.png"></a></td>
									</tr>
									<c:set var="i" value="${i+1}" />
								</c:when>
								<c:otherwise>
									<tr>
										<form action="./updateGroupAction.do" method="post">
											<td width="8px" style="padding: 2px;">${i} <input
												type="hidden" name="groupId" value="${group.groupId}">
											</td>
											<td style="padding: 2px;"><input type="text" name="name"
												value="${group.name}"></td>
											<td style="padding: 2px;"><a
												onmouseover="show('groupUsers_${group.groupId}')"
												onmouseout="hide('groupUsers_${group.groupId}')"
												href="./viewUsersByGroup.do?groupId=${group.groupId}">View
													Users</a></td>
											<td style="padding: 2px;"><input type="submit" value=""
												style="text-align: center; width: 73px; height: 39px; background: no-repeat; background-image: url('http://localhost:8080/hibernate/pic/saveUser.png'); background-position: center;">
											</td>
											<td style="padding: 2px; text-align: center;"><a
												href="./deleteGroupAction.do?groupId=${group.groupId}"><img
													width="35px" alt="Delete"
													src="/hibernate/pic/deleteUser.png"></a></td>
									</tr>
									</form>
								</c:otherwise>
							</c:choose>
						</c:forEach>
						<tr>
							<form method="post" action="./addGroupAction.do">
								<td></td>
								<td colspan="3"><input type="text" name="name"></td>
								<td><input
									style="background: url('http://localhost:8080/hibernate/pic/addUser.png'); width: 39px; height: 38px;"
									type="submit" value=""></td>
							</form>
						</tr>
					</tbody>
				</table>
				<br> <a href="./homeAction.do">View all Users</a>
				<c:forEach var="group" items="${requestScope.groups}">
					<div style="display: none;" id="groupUsers_${group.groupId}">
						<c:choose>
							<c:when test="${!empty usersByGroup[group]}">
								<br>
								<table style="float: none;" border="2px">
									<tbody>
										<tr align="center" valign="middle">
											<td width="8px" style="padding: 2px;">#</td>
											<td style="padding: 2px;">First Name</td>
											<td style="padding: 2px;">Last Name</td>
											<td style="padding: 2px;">Login</td>
										</tr>
										<c:set var="i" value="1" />
										<c:forEach var="user" items="${usersByGroup[group]}">
											<tr>
												<td width="8px" style="padding: 2px;">${i}</td>
												<td style="padding: 2px;">${user.firstName}</td>
												<td style="padding: 2px;">${user.lastName}</td>
												<td style="padding: 2px;">${user.login}</td>
											</tr>
											<c:set var="i" value="${i+1}" />
										</c:forEach>
									</tbody>
								</table>
							</c:when>
							<c:otherwise>
								<br>
								<div>This group contain no users</div>
							</c:otherwise>
						</c:choose>
					</div>
				</c:forEach>
			</div>
			<div style="width: 40%;"></div>
		</div>
		<div style="width: 100%; height: 33%; vertical-align: bottom;">
			<div style="width: 33%;"></div>
			<div style="width: 33%;"></div>
			<div style="width: 33%;"></div>
		</div>
	</div>
	</div>
</body>
</html>