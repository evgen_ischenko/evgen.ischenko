<%@ page import="javax.servlet.http.HttpSession"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="java.util.Enumeration"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<link rel="icon" type="image/png" href="/hibernate/pic/favicon.ico">
<title>Hibernate</title>
</head>
<body>
	<div style="width: 100%; height: 100%">

		<div style="width: 100%; height: 33%; vertical-align: top;">
			<div align="left" style="width: 33%; visibility: hidden">1</div>
			<div align="center" style="width: 33%; visibility: hidden">2</div>
			<div align="right" style="width: 33%; visibility: hidden">3</div>
		</div>

		<div style="vertical-align: middle; height: 33%;">
			<div style="width: 25%; visibility: hidden; float: left;">1</div>
			<div style="width: 35%; float: left;">
				<table style="float: none;" border="2px">
					<tbody>
						<tr><td align="center" valign="middle" colspan="5">${group.name}</td></tr>
						<tr align="center" valign="middle">
							<td width="8px" style="padding: 2px;">#</td>
							<td style="padding: 2px;">First Name</td>
							<td style="padding: 2px;">Last Name</td>
							<td style="padding: 2px;">Login</td>
							<td style="padding: 2px;">Delete</td>
						</tr>
						<c:set var="i" value="1" />
						<c:forEach var="user" items="${requestScope.users}">
							<tr>
								<td width="8px" style="padding: 2px;">${i}</td>
								<td style="padding: 2px;">${user.firstName}</td>
								<td style="padding: 2px;">${user.lastName}</td>
								<td style="padding: 2px;">${user.login}</td>
								<td style="padding: 2px; text-align: center;"><a
									href="./deleteUserAction.do?userId=${user.userId}&groupId=${group.groupId}"><img width="35px" alt="Delete"
										src="/hibernate/pic/deleteUser.png"></a></td>
							</tr>
							<c:set var="i" value="${i+1}" />
						</c:forEach>
					<!--<tr>
							<form method="post" action="./addUserAction.do">
							<td><input type="hidden" name="groupId" value="${group.groupId}"></td>
							<td><input type="text" name="fName"></td>
							<td><input type="text" name="lName"></td>
							<td><input type="text" name="login"></td>
							<td><input style="background:url('http://localhost:8080/hibernate/pic/addUser.png'); width: 39px; height: 38px;" type="submit" value=""></td>
							</form>
						</tr> -->
					</tbody>
				</table>
				<br>
				<a href="./viewGroupsAction.do">View All Groups</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="./homeAction.do"> View All Users</a>
			</div>
			<div style="width: 40%;"></div>
		</div>

		<div style="width: 100%; height: 33%; vertical-align: bottom;">
			<div style="width: 33%;"></div>
			<div style="width: 33%;"></div>
			<div style="width: 33%;"></div>
		</div>

	</div>
	</div>
</body>
</html>