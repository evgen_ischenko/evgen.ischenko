<%@ page import="javax.servlet.http.HttpSession"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="java.util.Enumeration"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<link rel="icon" type="image/png" href="/hibernate/pic/favicon.ico">
<title>Hibernate</title>
</head>
<body>
	<div style="width: 100%; height: 100%">

		<div style="width: 100%; height: 33%; vertical-align: top;">
			<div align="left" style="width: 33%; visibility: hidden">1</div>
			<div align="center" style="width: 33%; visibility: hidden">2</div>
			<div align="right" style="width: 33%; visibility: hidden">3</div>
		</div>

		<div style="vertical-align: middle; height: 33%;">
			<div style="width: 25%; visibility: hidden; float: left;">1</div>
			<div style="width: 35%; float: left;">
				<table style="float: none;" border="2px">
					<tbody>
						<tr align="center" valign="middle">
							<td width="8px" style="padding: 2px;">#</td>
							<td style="padding: 2px;">First Name</td>
							<td style="padding: 2px;">Last Name</td>
							<td style="padding: 2px;">Login</td>
							<td style="padding: 2px;">Group</td>
							<td style="padding: 2px;">Edit</td>
							<td style="padding: 2px;">Delete</td>
						</tr>
						<c:set var="i" value="1" />
						<c:forEach var="user" items="${requestScope.users}">
							<tr>
								<td width="8px" style="padding: 2px;">${i}</td>
								<c:choose>
									<c:when test="${param.userId == user.userId}">
										<form method="post" action="./updateUserAction.do">
											<td style="padding: 2px;"><input type="hidden"
												name="userId" value="${user.userId}"> <input
												type="text" name="first_name" value="${user.firstName}"></td>
											<td style="padding: 2px;"><input type="text"
												name="last_name" value="${user.lastName}"></td>
											<td style="padding: 2px;"><input type="text"
												name="login" value="${user.login}"></td>
											<td style="padding: 2px;"><select name="groupId">
													<option value="-1"></option>
													<c:forEach var="group" items="${requestScope.groups}">
														<option value="${group.groupId}">${group.name}</option>
													</c:forEach>
											</select></td>
											<td style="padding: 2px;"><input
												style="background: url('http://localhost:8080/hibernate/pic/saveUser.png'); width: 39px; height: 38px;"
												type="submit" value=""></td>
											<td style="padding: 2px; text-align: center;"><a
												href="./deleteUserAction.do?userId=${user.userId}"><img
													width="35px" alt="Delete"
													src="/hibernate/pic/deleteUser.png"></a></td>
										</form>
									</c:when>
									<c:otherwise>
										<td style="padding: 2px;">${user.firstName}</td>
										<td style="padding: 2px;">${user.lastName}</td>
										<td style="padding: 2px;">${user.login}</td>
										<td style="padding: 2px;">${groupUserMap[user].name}</td>
										<td style="padding: 2px;"><a
											href="./homeAction.do?userId=${user.userId}"><img
												width="35px" alt="Delete" src="/hibernate/pic/editUser.jpg"></a></td>
										<td style="padding: 2px; text-align: center;"><a
											href="./deleteUserAction.do?userId=${user.userId}"><img
												width="35px" alt="Delete"
												src="/hibernate/pic/deleteUser.png"></a></td>
									</c:otherwise>
								</c:choose>
							</tr>
							<c:set var="i" value="${i+1}" />
						</c:forEach>
						<tr>
							<form method="post" action="./addUserAction.do">
								<td></td>
								<td><input type="text" name="fName"></td>
								<td><input type="text" name="lName"></td>
								<td><input type="text" name="login"></td>
								<td><select name="groupIdNewUser">
										<option value="-1"></option>
										<c:forEach var="group" items="${requestScope.groups}">
											<option value="${group.groupId}">${group.name}</option>
										</c:forEach>
								</select></td>
								<td><input
									style="background: url('http://localhost:8080/hibernate/pic/addUser.png'); width: 39px; height: 38px;"
									type="submit" value=""></td>
							</form>
						</tr>
					</tbody>
				</table>
				<br> <a href="./viewGroupsAction.do">View Groups</a>
			</div>
			<div style="width: 40%;"></div>
		</div>

		<div style="width: 100%; height: 33%; vertical-align: bottom;">
			<div style="width: 33%;"></div>
			<div style="width: 33%;"></div>
			<div style="width: 33%;"></div>
		</div>

	</div>
	</div>
</body>
</html>