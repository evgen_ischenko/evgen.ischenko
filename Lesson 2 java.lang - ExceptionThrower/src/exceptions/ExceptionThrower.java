package exceptions;

public class ExceptionThrower {

	public MyException throwMyException () {
		MyException e = new MyException();
		return e;
	}
	
	public MyRuntimeException throwMyRuntimeException () {
		MyRuntimeException e = new MyRuntimeException();
		return e;
	}
	
}
