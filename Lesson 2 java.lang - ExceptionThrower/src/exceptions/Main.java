package exceptions;

public class Main {

	public static void main(String[] args) {

		ExceptionThrower thrower = new ExceptionThrower();
		thrower.throwMyException();
		thrower.throwMyRuntimeException();
		
	}

}
