import java.io.File;

public class FolderManager {

	public void createFolder (String name) {
		File folder = new File(name);
		if (folder.exists()){
			System.out.println("Folder with name \"" + name + "\" already exists");	
		} else {
				folder.mkdir();
				System.out.println("Folder created successfully");
		}
	}
	public void renameFolder (String old, String newName) {
		File oldFolder = new File(old);
		File newFolder = new File(newName);
		if (oldFolder.isDirectory()){
			oldFolder.renameTo(newFolder);
			System.out.println("Folder renamed successfully");
		} else {
			System.out.println("Folder with name \"" + old + "\" does not exists");
		}
	}
	public void deleteFolder (String name) {
		File folder = new File(name);
		if (folder.isDirectory()) {
			folder.delete();
		} else {
			System.out.println(name + " does not exist or not a floder");
		}
	}
	
}
