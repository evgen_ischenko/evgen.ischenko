
public class Main {

	private static int operation = 1;
	private static String name = "default";
	private static String oldName = "default";
	private static String newName = "default_new";
	
	public static void main(String[] args) {
	
		FolderManager manager = new FolderManager();
		
		try {
			name = args[0];
			
			try {
				operation = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
				newName = args[1];
				operation = Integer.parseInt(args[2]);
			}
			oldName = args[0];
			if (newName.equals("default_new")) {
				newName = args[2];
			}
		} catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
//			e.printStackTrace();
//			System.out.println("Exception handled, application still works");
		}
		
		switch (operation) {
		case 1: {
			manager.createFolder(name);
			break;
		}
		case 2: {
			manager.renameFolder(oldName, newName);
			break;
		}
		case 3: {
			manager.deleteFolder(name);
			break;
		}
		}
		
	}

}
