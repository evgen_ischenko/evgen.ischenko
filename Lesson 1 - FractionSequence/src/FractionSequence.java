
public class FractionSequence {

	public static void main (String args[]) {
		try {
			double n = Double.parseDouble(args[0]);

			for (double i=1; i < n+1; i++) {
				System.out.println(1/i);
			}

			} catch (IllegalArgumentException e) {
			System.out.println("Argument is wrong, should be integer.");
		}
	}
		
}
