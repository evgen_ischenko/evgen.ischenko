package threads;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

	/*
	 * Creates lots of folders with lots of files for a test purposes...
	 * Decrease 'dest' to decrease number of created folders and files.
	 */

public class TestFileCreator {

	public static List<String> list = new ArrayList<>();
	
	public static void main(String[] args) throws IOException {
		
		int dest = 20;
		int init = 1;
		String initName = "f";
		String name = "f";
		String fileName = "file_";
		String[] extension = new String[8];
		extension[0] = ".doc";
		extension[1] = ".java";
		extension[2] = ".txt";
		extension[3] = ".dat";
		extension[4] = ".avi";
		extension[5] = ".mp3";
		extension[6] = ".zip";
		extension[7] = ".exe";
		Random rnd = new Random();
		long folderNumber = 0;
		long fileNumber = 0;
		while (init <= dest) {
			
			for (int i = 0; i < init; i++) {
					list.add(name);
					File folder = new File(name + "_" + i);
					folder.mkdir();
					folderNumber++;
					for (int n = 0; n < 100; n++) {
						String str = folder.getPath()+ "\\" + fileName;
						int ext = rnd.nextInt(8);
						File file = new File(str + n + extension[ext]);
						file.createNewFile();
						fileNumber++;
					}	
			}
					int i = 0;
					for (String string : list) {
						File folder = new File(string + "_" + i);
						folder.mkdir();
						folderNumber++;
						for (int n = 0; n < 100; n++) {
							String str = folder.getPath()+ "\\" + fileName;
							int ext = rnd.nextInt(8);
							File file = new File(str + n + extension[ext]);
							file.createNewFile();
							fileNumber++;
						}	
						i++;
					}
			name = name + "_" + (init-1) + "\\" + initName;
			init++;
		}
		System.out.println("Created " + folderNumber + " folders");
		System.out.println("Created " + fileNumber + " files");
	}
	
}
