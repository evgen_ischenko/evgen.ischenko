package threads;

import java.io.File;

public class FolderSearcher {
	
	public void search(String path, String pattern) {
		
		File folder = new File(path);
		File[] folders = folder.listFiles();
		if (folders != null) {
		for (int i = 0; i < folders.length; i++) {
			if (folders[i].isDirectory()) {
				String folderName = folders[i].getAbsolutePath();
				Search search = new Search(folderName, pattern);
				new  Thread(search).start();
			}
		}
		}
	}
	
}
