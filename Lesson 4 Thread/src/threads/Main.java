package threads;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) throws IOException, InterruptedException {

		String pattern = "";
		String folderPath = "";
	
		BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
		
		while (true) {
			if (folderPath.isEmpty()) {
				System.out.println("Please, enter folder to search in:");
				folderPath = read.readLine();
			} else {
				break;
			}
		}
		while (true) {			
			if (pattern.isEmpty() || !pattern.contains("*.")) {
				System.out.println("Enter pattern to search for:");
				pattern = read.readLine();
			} else {
				break;
			}
		}
		long startTime = System.currentTimeMillis();
		Search search = new Search(folderPath, pattern);
		new  Thread(search).start();
		Thread.currentThread();
		while (true) {
			Thread.sleep(2);
			if (Thread.activeCount() == 1) {
				break;
			}
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Program took " + (endTime - startTime) + " miliseconds");
	}

}
