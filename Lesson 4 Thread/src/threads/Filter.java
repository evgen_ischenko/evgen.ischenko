package threads;


public class Filter {

	String pattern = "";
	
	public boolean accept(String name, String pattern) {
		try {
			this.pattern = pattern.split("\\*")[1];
		} catch (Exception e) {
			e.printStackTrace();
		}
		String lowercaseName = name.toLowerCase();
		if (lowercaseName.endsWith(this.pattern)) {
			return true;
		} else {
			return false;
		}
	}
}
