package threads;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileSearcher {
	
	public void search(String path, String pattern) {
		
		File folder = new File(path);
		List<File> fileList = new ArrayList<>();
		Filter filter = new Filter();
		File[] files = folder.listFiles();
		if (files != null) {
			for (int i = 0; i < files.length; i++) {
				if (filter.accept(files[i].getName(), pattern)) {
					fileList.add(files[i]);
				}
			}
		}
		Search.addFileNames(fileList);
	}
	
}
