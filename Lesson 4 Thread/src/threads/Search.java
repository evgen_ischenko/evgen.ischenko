package threads;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class Search implements Runnable {

	public static List<String> fileNames = new ArrayList<>();
	public static long runTime = 0;
	
	private String path = "";
	private String pattern = "";
	
	@Override
	public void run() {
//		long startTime = System.currentTimeMillis();
		FileSearcher fileSearcher = new FileSearcher();
		fileSearcher.search(path, pattern);
		FolderSearcher folderSearcher = new FolderSearcher();
		folderSearcher.search(path, pattern);
//		long endTime = System.currentTimeMillis();
//		System.out.println(Thread.currentThread().getName() + " Completed. It took " + (endTime - startTime) + " miliseconds.");
	}
	
	public Search (String path, String pattern){
		this.path = path;
		this.pattern = pattern;
	}
	
	public static synchronized void addFileNames (List<File> fileList) {
		Iterator<File> iterator = fileList.iterator();
		while (iterator.hasNext()) {
			File f = iterator.next();
			Search.fileNames.add(f.getAbsolutePath());
			System.out.println(f.getAbsolutePath());
		}
	}

	
}
