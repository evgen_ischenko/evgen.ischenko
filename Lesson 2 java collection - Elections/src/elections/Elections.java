package elections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Elections {
	
	private VoteBox voteBox = new VoteBox();
	private List<String> list = new ArrayList<String>();
	
	public void doVote (String candidate) {
		Vote vote = new Vote();
		vote.setVote(candidate);
		voteBox.add(vote);
	}
	
	public void getResults (String str) {
		Vote[] votes = voteBox.getVotes().clone();
		Map<String, Integer> results = new HashMap<String, Integer>();
		for (int i = 0; i < votes.length; i++) {
			Integer count = results.get(votes[i].getVote());
			if (null == count) {
				count = 1;
				results.put(votes[i].getVote(), count);
			} else {
				count++;
				results.put(votes[i].getVote(), count);
			}
		}
		if (!str.equalsIgnoreCase("Romney")) {
			if (null != results.get(str)) {
				System.out.println(str + " got " + results.get(str) + " votes.");
			} else System.out.println(str + " got no votes.");
		} else System.out.println("Romney didn't get any votes, thanks to magic vote box.");	
	}
}
