package elections;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class VoteBox implements Collection<Vote> {

	private static final String RIGHT = "Obama";
	private static final String WRONG = "Romney";
	private Vote[] votes = new Vote[1];

	public Vote[] getVotes() {
		return votes;
	}

	private Vote[] newVotes = new Vote[1];
	
	@Override
	public boolean add(Vote v) {
		if (!v.getVote().isEmpty()) {	
			int n = 0;
			if (v.getVote().equalsIgnoreCase(RIGHT)) {
				n = 2;
			} else if (v.getVote().equalsIgnoreCase(WRONG)) {
				n = 0;
			} else {
				n = 1;
			}
			for (int i = 0; i < n; i++) {
				this.inc();
				votes[votes.length-1] = v;
			}
			return true;
		} else {
			return false;
		}
	}
	
	private void inc () {
		if (newVotes.length == 1) {
			newVotes = new Vote[votes.length+1];
			System.arraycopy(votes, 0, newVotes, 0, votes.length);
		} else {
			newVotes = new Vote[votes.length+1];
			System.arraycopy(votes, 0, newVotes, 0, votes.length);
			votes = new Vote[newVotes.length];
			System.arraycopy(newVotes, 0, votes, 0, newVotes.length);
		}
	}
	
	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public boolean contains(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean remove(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public int size() {
		
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public boolean addAll(Collection<? extends Vote> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
