package elections;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		Elections elections = new Elections();
		Random rand = new Random();
		// Some random elections...
		for (int i = 0; i < 25; i++) {
			int r = rand.nextInt(5);
			switch (r) {
			case 0: {elections.doVote("Obama"); 		break;}
			case 1: {elections.doVote("Romney"); 		break;}
			case 2: {elections.doVote("Bugs Bunny"); 	break;}
			case 3: {elections.doVote("Schwarzenegger");break;}
			case 4: {elections.doVote("Chack Norris");  break;}
			}
		}
/*		
		elections.doVote("Obama");
		elections.doVote("Romney");
		elections.doVote("Bugs Bunny");
		elections.doVote("Schwarzenegger");
		elections.doVote("Schwarzenegger");
		elections.doVote("Romney");
		elections.doVote("Chack Norris");
		elections.doVote("Romney");
		elections.doVote("Obama");
		elections.doVote("Romney");
		elections.doVote("Romney");
		elections.doVote("Schwarzenegger");
		elections.doVote("Romney");
		elections.doVote("Obama");
		elections.doVote("Bugs Bunny");
		elections.doVote("Obama");
		elections.doVote("Bugs Bunny");
*/		
		elections.getResults("Obama");
		elections.getResults("Schwarzenegger");
		elections.getResults("Romney");
		elections.getResults("Bugs Bunny");
		elections.getResults("Chack Norris");
	}

}
