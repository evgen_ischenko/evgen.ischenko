package ua.ck.geekHub;

import java.util.*;

public class Stack<String> {

	private Queue<String> stack = new LinkedList<String>();
	
	public String poll() {
		this.reverse();
		String str = stack.poll();
		this.reverse();
		return str;
	}
	
	public String peek() {
		this.reverse();
		String str = stack.peek();
		this.reverse();
		return str;
	}
	
	public boolean add (String str) {
		try {
			stack.add(str);
			System.out.println("Added: " + this.peek());
			return true;
		} catch (NullPointerException error) {
			return false;			
		}
	}
	
	public Iterator iterator() {
		return stack.iterator();
	}
	
	private void reverse () {
		String[] array = (String[]) stack.toArray();
		int size = array.length;
		stack.clear();
		for (int i = size-1; i>=0; i--) {
			stack.add(array[i]);
		}
	}
	
}
