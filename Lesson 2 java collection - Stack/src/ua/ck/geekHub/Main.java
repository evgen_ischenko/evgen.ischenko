package ua.ck.geekHub;

public class Main {

	public static void main(String[] args) {
		Stack<String> stack = new Stack<String>();
		
		System.out.println("LIFO queue. Last element should be pulled first.");
		System.out.println("**************************************");	
		
		stack.add("Cat");
		stack.add("Dog");
		stack.add("Tiger");
		stack.add("Elephant");
		
		System.out.println("**************************************");
		System.out.println("Polled: " + stack.poll());
		System.out.println("Polled: " + stack.poll());
		System.out.println("Polled: " + stack.poll());
		System.out.println("Polled: " + stack.poll());
		System.out.println("**************************************");
		
		stack.add("Car");
		stack.add("Motorcycle");
		stack.add("Bus");
		
		System.out.println("**************************************");
		System.out.println("Peeked: " + stack.peek());
		System.out.println("Peeked: " + stack.peek());
		System.out.println("Peek method does not remove element from stack.");
	}
}
