/**
 * Created with IntelliJ IDEA.
 * User: Evgen
 * Date: 28.04.13
 * Time: 12:10
 * To change this template use File | Settings | File Templates.
 */
public class Main {

	public static void main(String args[]) {
		int number = 0;
		try {
			number = Integer.parseInt(args[0]);
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} finally {
			String[] words = {"zero",
					"one",
					"two",
					"three",
					"four",
					"five",
					"six",
					"seven",
					"eight",
					"nine",
					"ten"};
			System.out.println(number + " equals '" + words[number] + "'");
		}
	}

}
