package math;

public class Sum {

	public static void main(String[] args) {

		if (isDecimal(args[0]) && isDecimal(args[1])) {
			int dec1 = Integer.parseInt(args[0]);
			int dec2 = Integer.parseInt(args[1]);
			System.out.println("Decimal: " + (dec1 + dec2));
		}
		
		if (isHexadecimal(args[0]) && isHexadecimal(args[1])) {
			int hex1 = Integer.parseInt(args[0], 16);
			int hex2 = Integer.parseInt(args[1], 16);
			System.out.println("Hexadecimal: " + (hex1 + hex2));
		}
		
		if (isOctal(args[0]) && isOctal(args[1])) {
			int octal1 = Integer.parseInt(args[0], 8);
			int octal2 = Integer.parseInt(args[1], 8);
			System.out.println("Octal: " + (octal1 + octal2));
		}
	
		if (isFractial(args[0]) && isFractial(args[1])) {
			float fraction1 = Float.parseFloat(args[0]);
			float fraction2 = Float.parseFloat(args[1]);
			System.out.println("Fractial: " + (fraction1 + fraction2));
		}
		
	}

	public static boolean isHexadecimal(String s) {
		try {
			int hex = Integer.parseInt(s, 16);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
	
	public static boolean isOctal (String s) {
		try {
			int octal = Integer.parseInt(s, 8);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
	
	public static boolean isDecimal (String s) {
		try {
			int dec = Integer.parseInt(s);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
	
	public static boolean isFractial(String s) {
		try {
			float fraction = Float.parseFloat(s);
			return true;
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
	
}
