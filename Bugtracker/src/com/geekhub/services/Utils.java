package com.geekhub.services;

import com.geekhub.beans.TicketPriority;
import com.geekhub.beans.TicketStatus;

public class Utils {
	
	public static TicketPriority getPriority(String priority) {
		switch (priority) {
			case ("LOW"): return TicketPriority.LOW;
			case ("NORMAL"): return TicketPriority.NORMAL;
			case ("HIGH"): return TicketPriority.HIGH;
			default: return null;
		}
	}
	
	public static TicketStatus getStatus(String status) {
		switch (status) {
		case ("ACTIVE"): return TicketStatus.ACTIVE;
		case ("RESOLVED"): return TicketStatus.RESOLVED;
		case ("TESTED"): return TicketStatus.TESTED;
		case ("PUSHED"): return TicketStatus.PUSHED;
		default: return null;
		}
	}
	
}
