package com.geekhub.services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import com.geekhub.beans.Ticket;
import com.geekhub.beans.User;

public class BaseDao extends JdbcTemplate {

	public boolean saveTicket(Ticket ticket) {
		String sql ="INSERT INTO ticket (title, description, ownerId, status, priority) VALUES (" +
				" '" + ticket.getTitle() + "', " +
				" '" + 	ticket.getDescription() + "', " +
				" " + 	ticket.getOwner().getId() + ", " +
				" '" + 	ticket.getStatus() + "', " +
				" '" + 	ticket.getPriority() + "' " +
				")";
		int i = 0;
		i = this.update(sql);
		if (i == 0) {
			return false;
		}
		return true;
	}

	public int saveUser(User user) {
		String sql = "INSERT INTO user (login, name, password) VALUES (" +
				" '" + user.getLogin() + "', " +
				" '" + user.getName() + "', " +
				" '" + user.getPassword() + "' " +
				")";
		return this.update(sql);
	}
	
	public User getUserById(Integer id) {
		String sql = "SELECT * FROM user WHERE id = " + id;
		List<User> user = this.query(sql, new UserMapper());
		return user.get(0);
	}
	
	public Ticket getTicketById(Integer id) {
		String sql = "SELECT * FROM ticket WHERE id = " + id;
		List<Ticket> ticket = this.query(sql, new TicketMapper());
		return ticket.get(0);
	}
	
	public int updateTicket(Ticket ticket) {
		User user = ticket.getOwner();
		String sql ="UPDATE ticket SET " +
				" title       = " + " '" + ticket.getTitle() + "', " +
				" description = " + " '" + ticket.getDescription() + "', " +
				" ownerId     = " + "  " + user.getId() + ", " +
				" status      = " + " '" + ticket.getStatus() + "', " +
				" priority    = " + " '" + ticket.getPriority() + "' " +
				" WHERE id = " + ticket.getId();
		return this.update(sql);
	}
	
	public int updateUser(User user) {
		String sql ="UPDATE user SET " +
				" login    = '" + user.getLogin() + "', " +
				" name     = '" + user.getName() + "', " +
				" password = '" + user.getPassword() + "' " +
				" WHERE id = " + user.getId();
		return this.update(sql);
	}

	public List<Ticket> getAllTickets() {
		String sql = "SELECT * FROM ticket";
		List<Ticket> tickets = this.query(sql, new TicketMapper());
		for (Ticket ticket : tickets) {
			User user = getUserById(ticket.getOwner().getId());
			ticket.setOwner(user);
		}
		return tickets;
	}
	
	public List<User> getAllUsers() {
		String sql = "SELECT * FROM user";
		List<User> users = this.query(sql, new UserMapper());
		return users;
	}
	
	public void deleteUser(Integer id) {
		String sql = "DELETE FROM user WHERE id = " + id;
		this.update(sql);
	}

	public void deleteTicket(Integer id) {
		String sql = "DELETE FROM ticket WHERE id = " + id;
		this.update(sql);
	}
	
}

class TicketMapper implements RowMapper<Ticket> {

	@Override
	public Ticket mapRow(ResultSet rs, int rowNum) throws SQLException {
		Ticket ticket = new Ticket();
		ticket.setDescription(rs.getString("description"));
		ticket.setTitle(rs.getString("title"));
		ticket.setId(Integer.parseInt(rs.getString("id")));

		User owner = new User();
		owner.setId(rs.getInt("ownerId"));
		ticket.setOwner(owner);

		ticket.setPriority(Utils.getPriority(rs.getString("priority")));
		ticket.setStatus(Utils.getStatus(rs.getString("status")));

		return ticket;
	}

}

class UserMapper implements RowMapper<User> {

	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user = new User();
		user.setId(Integer.parseInt(rs.getString("id")));
		user.setName(rs.getString("name"));
		user.setPassword(rs.getString("password"));
		user.setLogin(rs.getString("login"));
		return user;
	}

}

