package com.geekhub.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.geekhub.beans.Ticket;
import com.geekhub.beans.User;
import com.geekhub.services.BaseDao;
import com.geekhub.services.Utils;

public class TicketController {

	@Autowired
	private BaseDao dao;
	
	@RequestMapping(value = "/saveTicket.html")
	public void save(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Ticket ticket = new Ticket();
		ticket.setTitle(request.getParameter("title"));
		ticket.setDescription(request.getParameter("description"));
		User owner = new User();
		owner = dao.getUserById(Integer.parseInt(request
				.getParameter("owner.id")));
		ticket.setOwner(owner);
		ticket.setPriority(Utils.getPriority(request.getParameter("priority")));
		ticket.setStatus(Utils.getStatus(request.getParameter("status")));
		if (request.getParameter("id") != null) {
			ticket.setId(Integer.parseInt(request.getParameter("id")));
			dao.updateTicket(ticket);
		} else {
			dao.saveTicket(ticket);
		}
		response.sendRedirect("./listTickets.html");
	}
	
	@RequestMapping(value = "/saveUser.html")
	public void saveUser(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		User user = new User();
		user.setLogin(request.getParameter("login"));
		user.setName(request.getParameter("name"));
		user.setPassword(request.getParameter("password"));
		if (request.getParameter("id") != null && !request.getParameter("id").equals("")) {
			user.setId(Integer.parseInt(request.getParameter("id")));
			dao.updateUser(user);
		} else {
			dao.saveUser(user);
		}
		response.sendRedirect("./listUsers.html");
	}

	@RequestMapping(value = "/listTickets.html")
	public ModelAndView list(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ModelAndView mav = new ModelAndView("tickets");
		List<Ticket> tickets = dao.getAllTickets();
		mav.addObject("tickets", tickets);
		return mav;
	}
	
	@RequestMapping(value = "/listUsers.html")
	public ModelAndView listUsers(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("users");
		List<User> users = dao.getAllUsers();
		mav.addObject("users", users);
		return mav;
	}
	
	@RequestMapping(value = "/loadTicket.html")
	public ModelAndView load(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ModelAndView mav = new ModelAndView("ticket");
		List<User> users = dao.getAllUsers();
		if (request.getParameter("id") != null) {
			Ticket ticket = dao.getTicketById(Integer.parseInt(request.getParameter("id")));
			mav.addObject("ticket", ticket);
		}
		mav.addObject("users", users);
		return mav;
	}
	
	@RequestMapping(value = "/loadUser.html")
	public ModelAndView loadUser(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ModelAndView mav = new ModelAndView("user");
		if (request.getParameter("id") != null && !request.getParameter("id").equals("")) {
			User user = dao.getUserById(Integer.parseInt(request.getParameter("id")));
			mav.addObject("user", user);
		}
		return mav;
	}

	@RequestMapping(value = "/deleteTicket.html")
	public void deleteTicket(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		response.sendRedirect("./listTickets.html");
	}
	
	@RequestMapping(value = "/deleteUser.html")
	public void deleteUser(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		dao.deleteUser(Integer.parseInt(request.getParameter("id")));
		response.sendRedirect("./listUsers.html");
	}
	
	@RequestMapping(value = "/js.html")
	public ModelAndView jsLearning(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ModelAndView mav = new ModelAndView("js");
		return mav;
	}

}
