<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<head>
<script type="text/javascript" src="js/additional-methods.js">
	
</script>
<script type="text/javascript" src="js/jquery.validate.js">
	
</script>
<script type="text/javascript" src="js/jquery-1.9.0.js">
	
</script>
<script type="text/javascript">
$(document).ready(function() {
	$("#user").validate();
});
</script>
</head>
<body>
	<form id="user" action="./saveUser.html">
	 <fieldset>
		Login <input type="text" value="${user.login}" name="login"><br>
		Password <input type="text" value="${user.password}" name="password"><br>
		Name <input class="required" minlength="2" type="text" value="${user.name}"
			name="name"><br> <input type="hidden" name="id"
			value="${user.id}"> <input type="submit" value="Save">
	</fieldset>		
	</form>
</body>
</html>