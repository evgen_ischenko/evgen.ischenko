package reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class BeanRepresenter {
	
	private Class<?> inspectedClass;
	
	public BeanRepresenter (String className) throws ClassNotFoundException {
		this.inspectedClass = Class.forName(className);
		MyObject obj = new MyObject();
		obj.setName(inspectedClass.getName());
		obj.setFields(inspectedClass.getFields());
		obj.setMethods(inspectedClass.getMethods());
		obj.setChildObjects(inspectedClass.getClasses());
		obj.setConstructors(inspectedClass.getConstructors());
		obj.getObject();
	}
	
	public static String getMethodParameters(Method m) {
		String params = "";
		Class<?>[] parameters = m.getParameterTypes();
		if (parameters.length == 1) {
			params = parameters[0].getSimpleName();
		} else {
			for (Class<?> c : parameters) {
				params = "" + c.getSimpleName();
			}
		}
		return params;
	}
	
	public static String getModificators(Object o) {
		int i = 0;
		String mods = "";
		// resolving type of object
		try {
			i = ((Method) o).getModifiers();
		} catch (ClassCastException e) {
			// do nothing
		}
		try {
			i = ((Class<?>) o).getModifiers();
		} catch (ClassCastException e) {
			// do nothing
		}
		try {
			i = ((Field) o).getModifiers();
		} catch (ClassCastException e) {
			// do nothing
		}
		try {
			i = ((Constructor<?>) o).getModifiers();
		} catch (ClassCastException e) {
			// do nothing
		}
		
		if (Modifier.isPrivate(i)) {
			mods += "private ";
		}
		if (Modifier.isProtected(i)) {
			mods += "protected ";
		}
		if (Modifier.isPublic(i)) {
			mods += "public ";
		}
		if (Modifier.isFinal(i)) {
			mods += "final ";
		}
		if (Modifier.isStatic(i)) {
			mods += "static ";
		}
		if (Modifier.isSynchronized(i)) {
			mods += "synchronized ";
		}
		if (Modifier.isAbstract(i)) {
			mods += "abstract ";
		}
		if (Modifier.isInterface(i)) {
			mods += "interface ";
		}
		if (Modifier.isNative(i)) {
			mods += "native ";
		}
		if (Modifier.isStrict(i)) {
			mods += "strict ";
		}
		if (Modifier.isTransient(i)) {
			mods += "transient ";
		}
		if (Modifier.isVolatile(i)) {
			mods += "volatile ";
		}
		return mods;
	}
	
}
