package reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class MyObject {

	private String name;
	private Method[] methods;
	private Field[] fields;
	private Constructor<?>[] constructors;
	private Class<?>[] childObjects;
	
	public void getObject() {
		System.out.println();
		System.out.println(name);
		if (fields.length > 0) {
			System.out.println(" Fields:");
			for (Field f : fields) {
				if (!AnnotationHandler.handle(f)) {
					System.out.println("  " + BeanRepresenter.getModificators(f) + "" 
											+ f.getType().getSimpleName() + " " + f.getName());
				}
			}
		}
		if (methods.length > 0) {
			System.out.println(" Methods:");
			for (Method m : methods) {
				if (!AnnotationHandler.handle(m)) {
					System.out.println("  " + BeanRepresenter.getModificators(m) + ""
							+ m.getReturnType().getSimpleName() + " "
							+ m.getName() + "(" + BeanRepresenter.getMethodParameters(m) + ");");
				}
			}
		}
		if (childObjects.length > 0) {
			System.out.println(" Child objects:");
			for (Class<?> c : childObjects) {
				if (!AnnotationHandler.handle(c)) {
					System.out.println("  " + c.getSimpleName());
				}
			}
		}
		if (constructors.length > 0) {
			System.out.println(" Constructors:");
			for (Constructor<?> c : constructors) {
				if (!AnnotationHandler.handle(c)) {
					System.out.println("  " + BeanRepresenter.getModificators(c) + ""
							+ c.getName());
				}
			}
		}
	}
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Method[] getMethods() {
		return methods;
	}
	
	public void setMethods(Method[] methods) {
		this.methods = methods;
	}
	
	public Field[] getFields() {
		return fields;
	}
	
	public void setFields(Field[] fields) {
		this.fields = fields;
	}
	
	public Class<?>[] getChildObjects() {
		return childObjects;
	}
	
	public void setChildObjects(Class<?>[] childObjects) {
		this.childObjects = childObjects;
	}

	public Constructor<?>[] getConstructors() {
		return constructors;
	}

	public void setConstructors(Constructor<?>[] constructors) {
		this.constructors = constructors;
	}
	
}
