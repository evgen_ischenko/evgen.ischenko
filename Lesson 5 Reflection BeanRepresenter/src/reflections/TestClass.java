package reflections;

import java.util.Date;

public class TestClass {

	
	public static final int SOME_CONSTANT = 10;
	public Integer someInt;
	public boolean hasNext = false;
	public java.util.Date  date = new Date();
	
	@MyAnnotation
	private String str = "some string";
	protected static Object someObjectVariable;
	
	public String getStr() {
		return str;
	}
	
	@MyAnnotation
	public final boolean isWhatever() {
		return false;
	} 
	
	public int calculateSomeThing() {
		return 10;
	}
	
	@MyAnnotation
	public void setStr(String str) {
		this.str = str;
	}
	
	
	public class ChildClass {
		
	}
	
	@MyAnnotation
	public class ChildClassTwo {
		
	}
	
}
