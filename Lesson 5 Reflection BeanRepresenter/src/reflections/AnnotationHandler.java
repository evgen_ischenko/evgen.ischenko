package reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class AnnotationHandler {

	public static boolean handle(Object o) {
		// resolving type of object
		try {
			Method object = (Method) o;
			if (object.isAnnotationPresent(MyAnnotation.class)) {
				return true;
			}
			return false;
		} catch (ClassCastException e) {
			// do nothing
		}
		try {
			Class<?> object = (Class<?>) o;
			if (object.isAnnotationPresent(MyAnnotation.class)) {
				return true;
			}
			return false;
		} catch (ClassCastException e) {
			// do nothing
		}
		try {
			Field object = (Field) o;
			if (object.isAnnotationPresent(MyAnnotation.class)) {
				return true;
			}
			return false;
		} catch (ClassCastException e) {
			// do nothing
		}
		try {
			Constructor<?> object = (Constructor<?>) o;
			if (object.isAnnotationPresent(MyAnnotation.class)) {
				return true;
			}
			return false;
		} catch (ClassCastException e) {
			// do nothing
		}
		return false;
	}
	
}
