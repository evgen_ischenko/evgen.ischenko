package org.socialnetwork.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.socialnetwork.bean.Country;
import org.socialnetwork.bean.Message;
import org.socialnetwork.bean.User;
import org.socialnetwork.dao.BaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

public class BaseAction {

	@Autowired
	protected BaseDao dao;

	@RequestMapping(value = "homeAction.do")
	public ModelAndView viewHomePage(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("homePage");
		User loggedUser = dao.getUserById((int) request.getSession()
				.getAttribute("userId"));
		request.getSession().setAttribute("loggedUser", loggedUser);
		mav.addObject("user", loggedUser);
		return mav;
	}
	
	@RequestMapping(value="writeMessageAction.do")
	public void sendMessage(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String message = request.getParameter("message");
		User from = dao.getUserById((int) request.getSession().getAttribute("userId"));
		User to = dao.getUserById(Integer.parseInt(request.getParameter("to")));
		Message msg = new Message();
		msg.setFrom(from);
		msg.setTo(to);
		msg.setMessage(message);
		msg.setDate(new Date());
		dao.insertMessage(msg);
	}
	
	@RequestMapping(value="viewMessagesAction.do")
	public ModelAndView viewMessages(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("messages");
		User loggedUser = dao.getUserById((int) request.getSession().getAttribute("userId"));
		List<Message> income = loggedUser.getIncomeMessages();
		for (Message message : income) {
			message.setFrom(dao.getUserById(message.getFrom().getEntityId()));
			message.setTo(dao.getUserById(message.getTo().getEntityId()));
		}
		List<Message> outcome = loggedUser.getOutcomeMessages();
		for (Message message : outcome) {
			message.setFrom(dao.getUserById(message.getFrom().getEntityId()));
			message.setTo(dao.getUserById(message.getTo().getEntityId()));
		}
		loggedUser.setOutcomeMessages(outcome);
		loggedUser.setIncomeMessages(income);
		mav.addObject("loggedUser", loggedUser);
		return mav;
	}

	@RequestMapping(value = "viewProfileAction.do")
	public ModelAndView viewUserPage(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("homePage");
		User loggedUser = dao.getUserById((int) request.getSession()
				.getAttribute("userId"));
		User user = dao.getUserById(Integer.parseInt(request
				.getParameter("userId")));
		mav.addObject("user", user);
		mav.addObject("loggedUser", loggedUser);
		return mav;
	}

	@RequestMapping(value = "loadFriendsListAction.do")
	public ModelAndView loadFriends(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("userList");
		User user = null;
		Integer userId;
		try {
			userId = Integer.parseInt(request.getParameter("userId"));
		} catch (NumberFormatException e) {
			userId = (int) request.getSession().getAttribute("userId");
		}
		user = dao.getUserById(userId);
		List<User> friends = dao.getFriends(user);
		mav.addObject("users", friends);
		return mav;
	}

	@RequestMapping(value = "loadIgnoreListAction.do")
	public ModelAndView loadIgnoreList(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("userList");
		User loggedUser = dao.getUserById((int) request.getSession()
				.getAttribute("userId"));
		List<User> ignoreList = dao.getIgnoreList(loggedUser);
		mav.addObject("users", ignoreList);
		return mav;
	}

	@RequestMapping(value = "peopleSearchAction.do")
	public ModelAndView search(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("search");
		mav.addObject("countries", dao.getCountries());
		if (null != request.getParameter("action")
				&& request.getParameter("action").equals("load")) {
			return mav;
		} else {
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String address = request.getParameter("address");
			String city = request.getParameter("city");
			Integer country = Integer.parseInt(request.getParameter("country"));
			List<User> results = dao.peopleSearch(firstName, lastName, address, city, country);
			mav.addObject("results", results);
			return mav;
		}
	}

	@RequestMapping(value = "addToFriendsAction.do")
	public void addToFriends(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		User user = (User) request.getSession().getAttribute("loggedUser");
		Integer friendId = Integer.parseInt(request.getParameter("userId"));
		User friend = dao.getUserById(friendId);
		dao.addUserToFriendsList(user, friend);
		response.sendRedirect("viewProfileAction.do?userId=" + friendId);
	}

	@RequestMapping(value = "editProfileAction.do")
	public ModelAndView editPersonalDataAction(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		User user = (User) request.getSession().getAttribute("loggedUser");
		String action = request.getParameter("action");
		switch (action) {
		default: {
			ModelAndView mav = new ModelAndView("register");
			List<Country> countries = dao.getCountries();
			mav.addObject("countries", countries);
//			mav.addObject("user", user);
			mav.addObject("action", "editProfileAction.do");
			return mav;
		}
		case "done": {
			user.setFirstName(request.getParameter("firstName"));
			user.setLastName(request.getParameter("lastName"));
			user.setUserName(request.getParameter("userName"));
			user.setPassword(request.getParameter("password"));
			user.setEmail(request.getParameter("email"));
			String country = request.getParameter("country");
			user.setCountry(dao.getCountry(Integer.parseInt(country)));
			user.setCity(request.getParameter("city"));
			user.setAddress(request.getParameter("address"));
			user.setSchool(request.getParameter("school"));
			String birthDate = request.getParameter("birthDate");
			if (birthDate != null && !birthDate.equals("")) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date date = sdf.parse(birthDate);
				user.setDateOfBirth(date);
			}
			dao.saveOrUpdateUser(user);
			response.sendRedirect("./homeAction.do");
		}
		return null;
		}
	}

}
