package org.socialnetwork.action;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.socialnetwork.bean.User;
import org.socialnetwork.chat.ChatMessage;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

public class ChatAction extends BaseAction {

	private static Set<User> usersInChat = new HashSet<User>();
	
	@RequestMapping(value = "enterChatAction.do")
	public ModelAndView enterChatAction(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("chat/chat");
		User user = (User) request.getSession().getAttribute("loggedUser");
		usersInChat.add(user);
		List<ChatMessage> chatMessages = dao.getChatHistory();
		mav.addObject("chatMessages", chatMessages);
		mav.addObject("usersInChat", usersInChat);
		return mav;
	}

	@RequestMapping(value = "leaveChatAction.do")
	public void leaveChatAction(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		User user = (User) request.getSession().getAttribute("loggedUser");
		usersInChat.remove(user);
		response.sendRedirect("./homeAction.do");
	}
	
	@RequestMapping(value = "sendMessageAction.do")
	public void sendMessageAction(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		User user = (User) request.getSession().getAttribute("loggedUser");
		Timestamp timestamp = new Timestamp(new java.util.Date().getTime());
		String message = request.getParameter("message");
		dao.insertChatMessage(user, message, timestamp);
		response.sendRedirect("./enterChatAction.do");
	}
	
	@RequestMapping(value = "refreshMsgListAction.do")
	public ModelAndView refreshMsgListAction(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		ModelAndView mav = new ModelAndView("chat/chatBody");
		List<ChatMessage> chatMessages = dao.getChatHistory();
		mav.addObject("chatMessages", chatMessages);
		return mav;
	}
	
	@RequestMapping(value = "refreshUserListAction.do")
	public ModelAndView refreshUserListAction(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		ModelAndView mav = new ModelAndView("chat/userList");
		mav.addObject("usersInChat", usersInChat);
		return mav;
	}
}
