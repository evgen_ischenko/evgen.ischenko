package org.socialnetwork.action;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.socialnetwork.bean.Like;
import org.socialnetwork.bean.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

public class LikeAction extends BaseAction {

	@RequestMapping(value = "getLikeFormAction.do")
	public ModelAndView getLikeStatus(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		User user = (User) request.getSession().getAttribute("loggedUser");
		Like like = new Like();
		like.setContent(request.getParameter("content"));
		like.setLikedContentId(request.getParameter("likedContentId"));
		like.setOwnerId(user.getEntityId());
		like.setUrl(request.getParameter("url"));
		like = dao.getLike(user, like);
		like.setCount(dao.getCountForLike(like.getId()));
		ModelAndView mav = new ModelAndView("likes/like");
		mav.addObject("like", like);
		return mav;
	}
	
	@RequestMapping(value = "doLikeAction.do")
	public ModelAndView doLike(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		User user = (User) request.getSession().getAttribute("loggedUser");
		
		Like like = new Like();
		like.setUrl(request.getParameter("url"));
		like.setLikedContentId(request.getParameter("likedContentId"));
		like.setContent(request.getParameter("content"));
		like.setOwnerId(user.getEntityId());
		like = dao.getLike(user, like);
		
		Integer status = Integer.parseInt(request.getParameter("status"));
		like.setStatus(status);
		switch (status) {
		case 0: {
			dao.updateStatusForLike(user, like);
			break;}
		case 1: {
			dao.updateStatusForLike(user, like);
			break;}
		case 2: {
			dao.updateStatusForLike(user, like);
			break;}
		}
		
		like.setCount(dao.getCountForLike(like.getId()));
		
		ModelAndView mav = new ModelAndView("likes/like");
		mav.addObject("like", like);
		return mav;
	}
	
}
