package org.socialnetwork.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.socialnetwork.bean.City;
import org.socialnetwork.bean.Country;
import org.socialnetwork.bean.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

public class LoginAction extends BaseAction {

	@RequestMapping(value = "loginAction.do")
	public ModelAndView loginAction(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		HttpSession session = request.getSession();
		String login = request.getParameter("login");
		String password = request.getParameter("password");
		if (login != null && password != null) {
			User user = dao.getUserForLogin(login, password);
			if (user != null) {
				session.setAttribute("userId", user.getEntityId());
				session.setAttribute("loggedUser", user);
				response.sendRedirect("./homeAction.do");
			}
		}
		return new ModelAndView("login");
	}

	@RequestMapping(value = "showLoginAction.do")
	public ModelAndView showLoginPage(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("login");
		return mav;
	}

	@RequestMapping(value = "/registerAction.do")
	public ModelAndView registerAction(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String action = request.getParameter("action");
		switch (action) {
		case "done": {
			ModelAndView mav = new ModelAndView("login");
			User user = new User();
			user.setFirstName(request.getParameter("firstName"));
			user.setLastName(request.getParameter("lastName"));
			user.setUserName(request.getParameter("userName"));
			user.setPassword(request.getParameter("password"));
			user.setEmail(request.getParameter("email"));
			String country = request.getParameter("country");
			if (country == null || country.equals("")) {
				country = "1";
			}
			user.setCountry(dao.getCountry(Integer.parseInt(country)));
			user.setCity(request.getParameter("city"));
			user.setAddress(request.getParameter("address"));
			user.setSchool(request.getParameter("school"));
			user.setAvatar(1);
			String birthDate = request.getParameter("birthDate");
			if (birthDate != null && !birthDate.equals("")) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date date = sdf.parse(birthDate);
				user.setDateOfBirth(date);
			}
			int i = dao.saveOrUpdateUser(user);
			if (i != 1) {
				request.setAttribute("errorMsg", "something went wrong");
				response.sendRedirect("./registerAction.do?action=load");
			}
			mav.addObject("message", "Registered successfully.");
			return mav;
		}
		default:
			ModelAndView modelAndView = new ModelAndView("register");
			List<Country> countries = dao.getCountries();
			modelAndView.addObject("countries", countries);
			modelAndView.addObject("action", "registerAction.do");
			return modelAndView;
		}
	}

	@RequestMapping(value = "/getCitiesForSuggestAction.do")
	public void getCitiesForSuggest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String city = request.getParameter("city");
		if (null != city && !city.equals("")) {
			List<City> cities = dao.getCitiesForSuggest(city);
			if (cities.size() > 0) {
				String citiesJson = "{";
				int i = 1;
				for (City c : cities) {
					citiesJson += "\"" + i + "\":\"" + c.getDescription() + "\",";
					i++;
				}
				citiesJson = citiesJson.substring(0, citiesJson.length() - 1);
				citiesJson += "}";

				response.getWriter().write(citiesJson);
			} else {
				response.getWriter().write("");
			}
		}
	}

}
