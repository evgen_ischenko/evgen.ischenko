package org.socialnetwork.action;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.rowset.serial.SerialException;
import javax.swing.text.html.ImageView;

import org.socialnetwork.bean.Picture;
import org.socialnetwork.bean.Post;
import org.socialnetwork.bean.User;
import org.socialnetwork.dao.BaseDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

public class FileAction {

	@Autowired
	private BaseDao dao;
	private final int MAX_WIDTH = 250;
	private final int MAX_HEIGHT = 250;

	@RequestMapping(value = "submitPostOnWallAction.do")
	public String processUploadPreview(
			@RequestParam("file") MultipartFile file,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, SerialException, SQLException {
		User loggedUser = (User) request.getSession()
				.getAttribute("loggedUser");
		Post post = new Post();
		if (file.getSize() > 0 && file.getSize() < 1024000) {
			byte[] fileData = file.getBytes();
			InputStream in = new ByteArrayInputStream(fileData);
			BufferedImage img = ImageIO.read(in);
			float width = img.getWidth();
			float height = img.getHeight();
			float multiplier = 1;
			if (MAX_WIDTH < width) {
				if (MAX_HEIGHT < height) {
					multiplier = (width > height) ? (MAX_WIDTH / width)	: (MAX_HEIGHT / height);
				} else {
					multiplier = MAX_WIDTH / width;
				}
			} else if (MAX_HEIGHT < height) {
				multiplier = MAX_HEIGHT / height;
			}
			
			Image resizedPic = img
					.getScaledInstance((int) (img.getWidth() * multiplier),
							(int) (img.getHeight() * multiplier),
							BufferedImage.SCALE_SMOOTH);
			img = new BufferedImage(resizedPic.getWidth(null),
					resizedPic.getHeight(null), BufferedImage.TYPE_3BYTE_BGR);
			img.getGraphics().drawImage(resizedPic, 0, 0, null);
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			
			ImageIO.write(img, file.getOriginalFilename().replaceAll("[a-zA-Z]*\\.", ""), baos);
			
			dao.insertPicture(loggedUser, baos.toByteArray(), file.getOriginalFilename(),
					width, height, multiplier);
			post.setPicture(dao.getAllPics().get(0));
		}
		post.setOwner(loggedUser);
		post.setVideo(null);
		post.setText(request.getParameter("postText"));
		post.setAsOfDate(new Timestamp(new Date().getTime()));
		User user = dao.getUserById(Integer.parseInt(request
				.getParameter("wallOwner")));
		post.setWall(user.getWall());
		dao.insertPost(post);
		if (loggedUser.getEntityId() == user.getEntityId()) {
			return "redirect:homeAction.do";
		} else {
			return "redirect:viewProfileAction.do?userId=" + user.getEntityId();
		}
	}

	@RequestMapping(value = "getImageAction.do")
	public void getImage(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		Picture picture = dao.getPictureById(Integer.parseInt(request
				.getParameter("pictureId")));
		response.setContentType("image/" + picture.getExtension());
		BufferedInputStream input = null;
		BufferedOutputStream output = null;
		byte[] buffer = new byte[1024];
		try {
			input = new BufferedInputStream(picture.getContent());
			output = new BufferedOutputStream(response.getOutputStream());
			for (int len = 0; (len = input.read(buffer)) > 0;) {
				output.write(buffer);
			}
		} finally {
			if (output != null)
				try {
					output.close();
				} catch (IOException e) {
				}
			if (input != null)
				try {
					input.close();
				} catch (IOException e) {
				}
		}
	}
}
