package org.socialnetwork.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.socialnetwork.bean.City;
import org.socialnetwork.bean.Country;
import org.socialnetwork.bean.Like;
import org.socialnetwork.bean.Message;
import org.socialnetwork.bean.Picture;
import org.socialnetwork.bean.Post;
import org.socialnetwork.bean.User;
import org.socialnetwork.bean.Video;
import org.socialnetwork.bean.Wall;
import org.socialnetwork.chat.ChatMessage;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class BaseDao extends JdbcTemplate {

	// ***********************************************
	// ********** Getters ****************************
	// ***********************************************
	public User getUserById(int userId) {
		String sql = "SELECT * FROM user u WHERE u.entityId = ?";
		return getUser(sql, new Object[] { userId });
	}

	public User getUserForLogin(String userName, String password) {
		String sql = "SELECT * FROM user u WHERE u.userName = ? AND u.password = ?";
		return getUser(sql, new Object[] { userName, password });
	}
	
	private User getUser(String sql, Object[] o) {
		List<User> users = query(sql, o, new UserMapper());
		if (users.size() > 0) {
			User user = users.get(0);
			user.setWall(getWallForUser(user));
			user.setFriends(getFriends(user));
			user.setIgnoreList(getIgnoreList(user));
			user.setIncomeMessages(getIncomeMessagesForUser(user));
			user.setOutcomeMessages(getOutcomeMessagesForUser(user));
			user.setCountry(getCountry(user.getCountry().getId()));
			return user;
		} else {
			return null;
		}
	}

	public Wall getWallForUser(User user) {
		String sql = "SELECT * FROM wall w WHERE w.userId = ?";
		Wall wall = query(sql, new Object[]{ user.getEntityId() }, new WallMapper()).get(0);
		wall.setPosts(getPostsforWall(wall));
		return wall;
	}

	public List<Post> getPostsforWall(Wall wall) {
		String sql = "SELECT * FROM post p WHERE p.wallId = ?";
		return query(sql, new Object[]{wall.getEntityId()}, new PostMapper());
	}
	
	public Picture getPictureById(int entityId) {
		String sql = "SELECT * FROM picture p WHERE p.entityId = ?";
		Picture picture = query(sql, new Object[]{ entityId },new PictureMapper()).get(0);
		return picture;
	}

	public Video getVideoById(int entityId) {
		String sql = "SELECT * FROM video v WHERE v.entityId = ?";
		Video video = query(sql, new Object[]{ entityId }, new VideoMapper()).get(0);
		return video;
	}

	public Post getPostById(int entityId) {
		String sql = "SELECT * FROM post p WHERE p.entityId = ?";
		Post post = query(sql, new Object[]{ entityId }, new PostMapper()).get(0);
		return post;
	}

	public List<Message> getOutcomeMessagesForUser(User user) {
		String sql = "SELECT * FROM message m WHERE m.fromId = ?";
		Object[] values = { user.getEntityId() };
		return getMessages(sql, values);
	}

	public List<Message> getIncomeMessagesForUser(User user) {
		String sql = "SELECT * FROM message m WHERE m.toId = ?";
		Object[] values = { user.getEntityId() };
		return getMessages(sql, values);
	}
	
	private List<Message> getMessages(String sql, Object[] values) {
		List<Message> messages = query(sql, values, new MessageMapper());
		return messages;
	}

	public Message getMessageById(int entityId) {
		String sql = "SELECT * FROM message m WHERE m.entityId = ?";
		Message message = query(sql, new Object[]{ entityId }, new MessageMapper()).get(0);
		return message;
	}

	public Wall getWallById(int entityId) {
		String sql = "SELECT * FROM wall w WHERE w.entityId = ?";
		Wall wall = query(sql, new Object[]{ entityId }, new WallMapper()).get(0);
		return wall;
	}

	public List<User> getIgnoreList(User user) {
		String sql = "SELECT * FROM ignoreList i "
				+ " INNER JOIN user u ON u.entityId = i.`ignoredId` "
				+ " WHERE i.userId = ?";
		List<User> ignoreList = query(sql, new Object[]{ user.getEntityId() }, new UserMapper());
		return ignoreList;
	}

	public List<User> getFriends(User user) {
		String sql = "SELECT u.* FROM friends f "
				+ " INNER JOIN user u ON u.entityId = f.`friendId` "
				+ " WHERE f.`userId` = ?";
		List<User> friends = query(sql, new Object[]{ user.getEntityId() }, new UserMapper());
		return friends;
	}

	public List<User> peopleSearch(String firstName, String lastName, String address, String city, Integer country) {
		String sql = "CALL `sn_peopleSearch` (?, ?, ?, ?, ?)";
		List<User> users = query(sql, new Object[]{firstName, lastName, address, city, country}, new UserMapper());
		return users;
	}

	public List<ChatMessage> getChatHistory() {
		String sql = "SELECT * FROM chatHistory ch"
				+ " ORDER BY ch.asOfDate DESC";
		List<ChatMessage> messages = query(sql, new ChatMessageMapper());
		for (ChatMessage chatMessage : messages) {
			User user = chatMessage.getAuthor();
			user = getUserById(user.getEntityId());
			chatMessage.setAuthor(user);
		}
		return messages;
	}

	public List<Country> getCountries() {
		String sql = "SELECT * FROM country";
		List<Country> countries = query(sql, new CountryMapper());
		return countries;
	}

	public Country getCountry(Integer id) {
		String sql = "SELECT * FROM country c WHERE c.ID = ?";
		Country country = query(sql, new Object[]{ id }, new CountryMapper()).get(0);
		return country;
	}
	
	public List<Picture> getAllPics() {
		return query("SELECT * FROM picture ORDER BY entityId DESC", new PictureMapper());
	}
	
	// TODO create table City and alter user table properly
	public List<City> getCitiesForSuggest(String city) {
		String sql = "SELECT DISTINCT u.city FROM user u WHERE u.city LIKE CONCAT(?, '%')";
		return query(sql, new Object[]{city}, new CityMapper());
	}
	
	public int getCountForLike(int likeId) {
		String sql = "CALL sn_like_getCount (?)";
		return query(sql, new Object[]{likeId}, new LikeCountMapper()).get(0);
	}
	
	public Like getLike(User user, Like like) {
		String sql = "SELECT * FROM likedContent lc WHERE lc.url = ? AND lc.likedContentId = ?";
		Object[] criteria = {
				like.getUrl(), like.getLikedContentId()
		};
		List<Like> l = query(sql, criteria, new LikeMapper());
		if (l.size() == 0) {
			sql = "INSERT INTO likedContent (url, likedContentId, content)" +
					"VALUES (?, ?, ?)";
			criteria = new Object[]{
					like.getUrl(), like.getLikedContentId(), like.getContent()
			};
			update(sql, criteria);
			sql = "SELECT lc.id from likedContent lc ORDER BY lc.id DESC LIMIT 1";
			like.setId(query(sql, new LikeIdMapper()).get(0));
			like.setStatus(0);
			insertUserLikeRelation(user, like);
			return like;
		} else {
			like.setId(l.get(0).getId());
			sql = "SELECT ulr.status FROM userLikeRelation ulr " +
					"WHERE ulr.userId = ? AND ulr.likedId = ?";
			criteria = new Object[]{
					user.getEntityId(), like.getId()
			};
			List<Integer> statuses = query(sql, criteria, new LikeStatusMapper());
			if (statuses.size() == 0) {
				like.setStatus(0);
				insertUserLikeRelation(user, like);
				return like;
			} else {
				like.setStatus(statuses.get(0));
				return like;
			}
		}
	}
	
	// ***********************************************
	// ********** Setters ****************************
	// ***********************************************
	public int insertMessage(Message message) {
		message.setMessage(message.getMessage().replaceAll("'", "\\'"));
		String sql = "INSERT INTO message (fromId, toId, message, asOfDate) VALUES (?, ?, ?, ?)";
		Object[] values = {
			message.getFrom().getEntityId(),
			message.getTo().getEntityId(),
			message.getMessage(),
			message.getDate()
		};
		return update(sql, values);
	}
	
	public int insertPicture(User owner, byte[] picture, String name, float width, float height, float multiplier) {
		String sql = "INSERT INTO picture (ownerId, picture, name, width, height, multiplier) VALUES (?, ?, ?, ?, ?, ?)";
		return update(sql, new Object[]{owner.getEntityId(), picture, name, width, height, multiplier});
	}

	public int addUserToIgnoreList(User user, User toIgnore) {
		String sql = "INSERT INTO ignoreList (userId, ignoredId) VALUES (?, ?)";
		return update(sql, new Object[]{user.getEntityId(), toIgnore.getEntityId()});
	}

	public int addUserToFriendsList(User user, User friend) {
		String sql = "INSERT INTO friends (userId, friendId) VALUES (?, ?)";
		return update(sql, new Object[]{user.getEntityId(), friend.getEntityId()});
	}

	public int insertChatMessage(User author, String message,
			Timestamp timestamp) {
		message = message.replaceAll("'", "\\'");
		String sql = "INSERT INTO chatHistory (asOfDate, authorId, message) VALUES (?, ?, ?)";
		return update(sql, new Object[]{timestamp, author.getEntityId(), message});
	}
	
	public int insertPost(Post post) {
		String sql = "INSERT INTO post (userId, pictureId, videoId, text, asOfDate, wallId) VALUES (?, ?, ?, ?, ?, ?)";
		Object[] values = {
				post.getOwner().getEntityId(),
				(post.getPicture() == null ? null : post.getPicture().getEntityId()),
				(post.getVideo() == null ? null : post.getVideo().getEntityId()),
				post.getText(),
				post.getAsOfDate(),
				(post.getWall() == null ? null : post.getWall().getEntityId())
		};
		return update(sql, values);
	}

	public int saveOrUpdateUser(User user) {
		String sql = null;
		Object[] criteria = {
				user.getFirstName(), user.getLastName(), user.getUserName(),
				user.getPassword(), user.getEmail(), user.getCountry().getId(),
				user.getCity(), user.getAddress(), user.getDateOfBirth(), user.getSchool(), user.getAvatar()
		};
		if (getUserById(user.getEntityId()) != null) {
			sql = "UPDATE user SET firstName = ?, lastName = ?, userName = ?, password = ?"
					+ ", email = ?, country = ?, city = ?, address = ?, dateOfBirth = ?, school = ?, avatar = ?"
					+ " WHERE entityId = " + user.getEntityId();
		} else {
			sql = "INSERT INTO user (firstName, lastName, userName, password, email, country, city, address, dateOfBirth, school, avatar) " +
					"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
		}
		try {
			int i = update(sql, criteria);
			return i;
		} catch (DuplicateKeyException e) {
			return 0;
		}
	}
	
	public void updateStatusForLike(User user, Like like) {
		String sql = "UPDATE userLikeRelation SET status = ? WHERE userId = ? AND likedId = ?";
		Object[] criteria = {
				like.getStatus(), user.getEntityId(), like.getId()
		};
		update(sql, criteria);
	}
	
	public void insertUserLikeRelation(User user, Like like) {
		String sql = "INSERT INTO userLikeRelation (userId, likedId, status) " +
				"VALUES (?, ?, ?)";
		Object[] criteria = {
				user.getEntityId(), like.getId(), like.getStatus()
		};
		update(sql, criteria);
	}
	
	public int saveOrUpdateLike(Like like) {
		String sql = "CALL sn_like_saveOrUpdateLike (?, ?, ?, ?, ?, ?)";
		Object[] criteria = {
				like.getUrl(), like.getLikedContentId(), like.getContent(), 
				like.getOwnerId(), like.getId(), like.getStatus()
		};
		return update(sql, criteria);
	}
}

/*
 * ************************************************
 * ************** Row Mappers *********************
 * ************************************************
 */
class UserMapper implements RowMapper<User> {
	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user = new User();
		user.setEntityId(rs.getInt("entityId"));
		user.setUserName(rs.getString("userName"));
		user.setEmail(rs.getString("email"));
		user.setFirstName(rs.getString("firstName"));
		user.setLastName(rs.getString("lastName"));
		user.setPassword(rs.getString("password"));
		user.setCity(rs.getString("city"));
		user.setAddress(rs.getString("address"));
		user.setDateOfBirth(rs.getDate("dateOfBirth"));
		user.setAvatar(rs.getInt("avatar"));
		if (rs.getInt("country") != 0) {
			Country country = new Country();
			country.setId(rs.getInt("country"));
			user.setCountry(country);
		}
		return user;
	}
}

class WallMapper implements RowMapper<Wall> {
	@Override
	public Wall mapRow(ResultSet rs, int rowNum) throws SQLException {
		Wall wall = new Wall();
		wall.setEntityId(rs.getInt("entityId"));
		return wall;
	}
}

class PictureMapper implements RowMapper<Picture> {
	@Override
	public Picture mapRow(ResultSet rs, int rowNum) throws SQLException {
		Picture picture = new Picture();
		picture.setEntityId(rs.getInt("entityId"));
		User owner = new User();
		owner.setEntityId(rs.getInt("ownerId"));
		picture.setOwner(owner);
		picture.setName(rs.getString("name"));
		picture.setContent(rs.getBinaryStream("picture"));
		picture.setWidth(rs.getInt("width"));
		picture.setHeight(rs.getInt("height"));
		picture.setMultiplier(rs.getFloat("multiplier"));
		return picture;
	}
}

class VideoMapper implements RowMapper<Video> {
	@Override
	public Video mapRow(ResultSet rs, int rowNum) throws SQLException {
		Video video = new Video();
		video.setEntityId(rs.getInt("entityId"));
		User owner = new User();
		owner.setEntityId(rs.getInt("ownerId"));
		video.setOwner(owner);
		video.setPath(rs.getString("path"));
		return video;
	}
}

class PostMapper implements RowMapper<Post> {
	@Override
	public Post mapRow(ResultSet rs, int rowNum) throws SQLException {
		Post post = new Post();
		post.setEntityId(rs.getInt("entityId"));
		User owner = new User();
		owner.setEntityId(rs.getInt("userId"));
		post.setOwner(owner);
		if (rs.getInt("pictureId") != 0) {
			Picture picture = new Picture();
			picture.setEntityId(rs.getInt("pictureId"));
			post.setPicture(picture);
		}
		if (rs.getInt("videoId") != 0) {
			Video video = new Video();
			video.setEntityId(rs.getInt("videoId"));
			post.setVideo(video);
		}
		post.setText(rs.getString("text"));
		Wall wall = new Wall();
		wall.setEntityId(rs.getInt("wallId"));
		post.setWall(wall);
		return post;
	}
}

class MessageMapper implements RowMapper<Message> {
	@Override
	public Message mapRow(ResultSet rs, int rowNum) throws SQLException {
		Message message = new Message();
		message.setEntityId(rs.getInt("entityId"));
		User from = new User();
		from.setEntityId(rs.getInt("fromId"));
		message.setFrom(from);
		User to = new User();
		to.setEntityId(rs.getInt("toId"));
		message.setTo(to);
		message.setMessage(rs.getString("message"));
		message.setDate(rs.getDate("asOfDate"));
		return message;
	}
}

class ignoreListMapper implements RowMapper<User> {
	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User ignored = new User();
		ignored.setEntityId(rs.getInt("ignoredId"));
		ignored.setFirstName(rs.getString("firstName"));
		ignored.setLastName(rs.getString("lastName"));
		return ignored;
	}
}

class FriendsMapper implements RowMapper<User> {
	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User friend = new User();
		friend.setEntityId(rs.getInt("friendId"));
		friend.setEmail(rs.getString("email"));
		friend.setFirstName(rs.getString("firstName"));
		friend.setLastName(rs.getString("lastName"));
		return friend;
	}
}

class ChatMessageMapper implements RowMapper<ChatMessage> {
	@Override
	public ChatMessage mapRow(ResultSet rs, int rowNum) throws SQLException {
		ChatMessage message = new ChatMessage();
		message.setEntityId(rs.getInt("entityId"));
		message.setTimestamp(rs.getTimestamp("asOfDate"));
		User author = new User();
		author.setEntityId(rs.getInt("authorId"));
		message.setAuthor(author);
		message.setMessage(rs.getString("message"));
		return message;
	}
}

class CountryMapper implements RowMapper<Country> {
	@Override
	public Country mapRow(ResultSet rs, int rowNum) throws SQLException {
		Country country = new Country();
		country.setId(rs.getInt("ID"));
		country.setDescription(rs.getString("description"));
		return country;
	}
}

class CityMapper implements RowMapper<City> {
	@Override
	public City mapRow(ResultSet rs, int rowNum) throws SQLException {
		City city = new City();
		city.setDescription(rs.getString("city"));
		return city;
	}
}

class LikeMapper implements RowMapper<Like> {
	@Override
	public Like mapRow(ResultSet rs, int rowNum) throws SQLException {
		Like like = new Like();
		like.setId(rs.getInt("id"));
		like.setUrl(rs.getString("url"));
		like.setLikedContentId(rs.getString("likedContentId"));
		like.setContent(rs.getString("content"));
		return like;
	}
}

class LikeCountMapper implements RowMapper<Integer> {
	@Override
	public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
		return rs.getInt("count");
	}
}

class LikeStatusMapper implements RowMapper<Integer> {
	@Override
	public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
		return rs.getInt("status");
	}
}

class LikeIdMapper implements RowMapper<Integer> {
	@Override
	public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
		return rs.getInt("id");
	}
}