package org.socialnetwork.bean;

import java.io.InputStream;

public class Picture extends Entity {

	private String name;
	private String extension;
	private User owner;
	private InputStream content;
	private int height;
	private int width;
	private float multiplier;

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public float getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(float multiplier) {
		this.multiplier = multiplier;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		setExtension();
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension() {
		this.extension = this.name.replaceAll("[a-zA-Z]*\\.", "");
	}

	public InputStream getContent() {
		return content;
	}

	public void setContent(InputStream content) {
		this.content = content;
	}

}
