package org.socialnetwork.bean;

import java.util.List;

public class Wall extends Entity {

	private List<Post> posts;

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

}
