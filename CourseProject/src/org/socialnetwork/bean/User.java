package org.socialnetwork.bean;

import java.util.Date;
import java.util.List;

public class User extends Entity {

	private String userName;
	private String email;
	private String firstName;
	private String lastName;
	private Wall wall;
	private List<User> friends;
	private List<User> ignoreList;
	private String password;
	private List<Message> incomeMessages;
	private List<Message> outcomeMessages;
	private Country country;
	private String city;
	private String address;
	private Date dateOfBirth;
	private String school;
	private int avatar;

	public int getAvatar() {
		return avatar;
	}

	public void setAvatar(int avatar) {
		this.avatar = avatar;
	}

	public List<User> getFriends() {
		return friends;
	}

	public void setFriends(List<User> friends) {
		this.friends = friends;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String fName) {
		this.firstName = fName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lNamel) {
		this.lastName = lNamel;
	}

	public Wall getWall() {
		return wall;
	}

	public void setWall(Wall wall) {
		this.wall = wall;
	}

	public List<Message> getIncomeMessages() {
		return incomeMessages;
	}

	public void setIncomeMessages(List<Message> incomeMessages) {
		this.incomeMessages = incomeMessages;
	}

	public List<Message> getOutcomeMessages() {
		return outcomeMessages;
	}

	public void setOutcomeMessages(List<Message> outcomeMessages) {
		this.outcomeMessages = outcomeMessages;
	}

	public List<User> getIgnoreList() {
		return ignoreList;
	}

	public void setIgnoreList(List<User> ignoreList) {
		this.ignoreList = ignoreList;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

}
