/**
 * 
 */
package org.socialnetwork.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GatewayFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		if (allowedUrl(httpRequest) || isLoggedIn(httpRequest)) {
			chain.doFilter(request, response);
		} else {
			httpResponse.sendRedirect("loginAction.do");
		}
	}

	private boolean isLoggedIn(HttpServletRequest request) {
		return null != request.getSession().getAttribute("loggedUser");
	}

	private boolean allowedUrl(HttpServletRequest request) {
		String uri = request.getRequestURI();
		if (uri.endsWith("loginAction.do") || uri.endsWith("registerAction.do")
				|| uri.endsWith(".js") || uri.endsWith(".css") || uri.endsWith("SuggestAction.do")) {
			return true;
		}
		return false;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
