/*
 * I don't know why it failes when running from eclipse,
 * but it is ok when executing in sql manager
 */

USE sn;

CREATE DEFINER = 'root'@'localhost' TRIGGER `createWallForNewUser` AFTER INSERT ON `user`
  FOR EACH ROW
BEGIN
	SET @usr = (SELECT u.entityId FROM user u ORDER BY u.entityId DESC LIMIT 1);
	
	INSERT INTO wall (userId)
	VALUES (@usr);
END;