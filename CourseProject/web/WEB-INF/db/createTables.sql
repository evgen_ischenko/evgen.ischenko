CREATE DATABASE `sn`;

USE `sn`;

SET GLOBAL max_allowed_packet = 1024*1024*14;

DROP TABLE user;

/*
 * Creating user table
 */
CREATE table user (
	entityId int NOT NULL AUTO_INCREMENT, 
	userName varchar(255) NOT NULL,
	email varchar(255),
	firstName varchar(255),
	lastName varchar(255),
	password varchar(255),
	country int,
	city varchar(255),
	address varchar(255),
	dateOfBirth date,
	school varchar(255),
	avatar int NOT NULL DEFAULT 1,
	PRIMARY KEY (entityId), 
	UNIQUE (userName)
);

DROP TABLE country;

/*
 * Creating country table
 */
CREATE TABLE country (
	ID int NOT NULL AUTO_INCREMENT,
	description varchar(30) NOT NULL,
	PRIMARY KEY (ID)
); 

DROP TABLE school;

/*
 * Creating school table
 */
CREATE TABLE school(
	ID int NOT NULL AUTO_INCREMENT,
	description varchar(30) NOT NULL,
	PRIMARY KEY (ID)
);


DROP TABLE friends;

/*
 * Creating friends table
 */
CREATE table friends (
	entityId int NOT NULL AUTO_INCREMENT,
	userId int,
	friendId int,
	PRIMARY KEY (entityId)
);

DROP TABLE comment;

/*
 * Creating comment table
 */
CREATE table comment (
	entityId int NOT NULL AUTO_INCREMENT,
	comment varchar(255) NOT NULL,
	PRIMARY KEY (entityId)
);

DROP TABLE picture;

/*
 * Creating picture table
 */
CREATE TABLE picture (
	entityId int NOT NULL AUTO_INCREMENT,
	ownerId int NOT NULL,
	picture BLOB,
	name varchar(30),
	width int,
	heigth int,
	multiplier float(9, 3),
	PRIMARY KEY (entityId)
);

DROP TABLE video;

/*
 * Creating video table
 */
CREATE TABLE video (
	entityId int NOT NULL AUTO_INCREMENT,
	ownerId int NOT NULL,
	video BLOB,
	name varchar(30),
	PRIMARY KEY (entityId)
);

DROP TABLE message;

/*
 * Creating message table
 */
CREATE TABLE message (
	entityId int NOT NULL AUTO_INCREMENT,
	fromId int,
	toId int,
	message varchar(255),
	asOfDate timestamp,
	PRIMARY KEY (entityId)
);

DROP TABLE wall;

/*
 * Creating wall table
 */
CREATE TABLE wall (
	entityId int NOT NULL AUTO_INCREMENT,
	userId int NOT NULL,
	UNIQUE(userId),
	PRIMARY KEY (entityId)
);

DROP TABLE postWallRelation;

/*
 * Creating postWallRelation table
 */
CREATE TABLE postWallRelation (
	entityId int NOT NULL AUTO_INCREMENT,
	wallId int,
	postId int,
	PRIMARY KEY (entityId)
);

DROP TABLE post;

/*
 * Creating post table
 */
CREATE TABLE post (
	entityId int NOT NULL AUTO_INCREMENT,
	userId int,
	pictureId int,
	videoId int,
	text varchar(255),
	asOfDate DATETIME,
	wallId int,
	PRIMARY KEY (entityId)
);

DROP TABLE postCommentRelation;

/*
 * Creating postCommentRelation table
 */
CREATE TABLE postCommentRelation (
	entityId int NOT NULL AUTO_INCREMENT,
	postId int,
	commentId int,
	asOfDate DATETIME,
	PRIMARY KEY (entityId)
);

DROP TABLE ignoreList;

/*
 * Create ignoreList table
 */
CREATE table ignoreList (
	entityId int NOT NULL AUTO_INCREMENT,
	userId int,
	ignoredId int,
	PRIMARY KEY (entityId)
);

DROP TABLE chatHistory;

/*
 * Create chatHistory Table
 */
CREATE TABLE chatHistory (
	entityId int NOT NULL AUTO_INCREMENT,
	asOfDate DATETIME,
	authorId int,
	message varchar(255),
	PRIMARY KEY(entityId)
);

/*
 * Some tables for 'like' system
 */

DROP TABLE likedContent;

CREATE TABLE likedContent (
	id int not null AUTO_INCREMENT,
	url varchar(255),
	likedContentId varchar(255),
	content varchar(1000) character set UTF8,
	PRIMARY KEY(id)
);

DROP TABLE userLikeRelation ();

CREATE TABLE userLikeRelation (
	id int not null AUTO_INCREMENT,
	userId int,
	likedId varchar(255),
	status int(1),
	PRIMARY KEY(id)
);
