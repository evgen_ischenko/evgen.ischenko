USE sn;

DROP PROCEDURE `sn_peopleSearch`;

CREATE DEFINER = 'root'@'localhost' PROCEDURE `sn_peopleSearch`(
        IN firstName VARCHAR(255),
        IN lastName VARCHAR(255),
        IN address VARCHAR(255),
        IN city VARCHAR(255), 
        IN countryId int
    )
    NOT DETERMINISTIC
    READS SQL DATA
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
(SELECT * 
	FROM user u
    WHERE
    (CASE firstName
    	WHEN '' THEN u.firstName LIKE '%'
    ELSE 
    	u.firstName LIKE CONCAT(firstName, '%')
    END)
	AND
    (CASE lastName
    	WHEN '' THEN u.lastName LIKE '%'
    ELSE
		u.lastName LIKE CONCAT(lastName, '%')
    END)
	AND
    (CASE address 
    	WHEN '' THEN u.address LIKE '%'
	ELSE
    	u.address LIKE CONCAT(address, '%')
    END)
	AND
    (CASE city
    	WHEN '' THEN u.city LIKE '%'
    ELSE
		u.city LIKE CONCAT(city, '%')
    END)
	AND
    (CASE countryId
	    WHEN -1 THEN u.country > countryId
    ELSE
		u.country = countryId
    END))

UNION

(SELECT * 
	FROM user u
    WHERE
    (CASE firstName
    	WHEN '' THEN u.firstName LIKE '%'
    ELSE 
    	u.firstName LIKE CONCAT('%', firstName, '%')
    END)
	AND
    (CASE lastName
    	WHEN '' THEN u.lastName LIKE '%'
    ELSE
		u.lastName LIKE CONCAT('%', lastName, '%')
    END)
	AND
    (CASE address 
    	WHEN '' THEN u.address LIKE '%'
	ELSE
    	u.address LIKE CONCAT('%', address, '%')
    END)
	AND
    (CASE city
    	WHEN '' THEN u.city LIKE '%'
    ELSE
		u.city LIKE CONCAT('%', city, '%')
    END)
    AND
    (CASE countryId
	    WHEN -1 THEN u.country > countryId
    ELSE
		u.country = countryId
    END));
END;