CREATE DEFINER = 'root'@'localhost' PROCEDURE `sn_like_getCount`(
        IN likedId INTEGER(11)
    )
    NOT DETERMINISTIC
    READS SQL DATA
    SQL SECURITY DEFINER
    COMMENT ''
BEGIN
	
	DROP TEMPORARY TABLE IF EXISTS countsHolder;

	CREATE TEMPORARY TABLE countsHolder (
    	rockHolder INTEGER(11),
        hateHolder INTEGER(11)
    );
    
    SET @rock = (SELECT Count(id) FROM userlikerelation ulr
        WHERE ulr.likedId = likedId AND ulr.status = 1);
        
    SET @hate = (SELECT Count(id) FROM userlikerelation ulr
        WHERE ulr.likedId = likedId AND ulr.status = 2);
    
    INSERT INTO countsHolder (rockHolder, hateHolder)
    VALUES (@rock, @hate);

	SELECT (h.rockHolder - h.hateHolder) as count
    FROM countsHolder h;
    
END;