/* Data for the 'user' table  (Records 1 - 3) */

use sn;

INSERT INTO `user` (`userName`, `email`, `firstName`, `lastName`, `password`, `country`, `city`, `address`, `dateOfBirth`, `school`) VALUES   ('ff.user', 'ff.user@browser.com', 'firefox', 'user', '123456', 1, '', '', '1989-10-28', 'null'),
  ('john.doe', 'john.doe@domain.com', 'John', 'Doe', '123456', -1, '', '', '1989-10-28', ''),
  ('someUserName', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);