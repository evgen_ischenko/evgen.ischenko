<%@include file="/WEB-INF/jsp/header.jsp"%>
<table>
	<form method="get" action="./peopleSearchAction.do?action=load">
		<tbody>
			<tr>
				<td></td>
				<td><span>First Name: </span></td>
				<td><span>Last Name: </span></td>
				<td><span>Address: </span></td>
				<td><span>City: </span></td>
				<td><span>Country: </span></td>
				<td></td>
			</tr>
			<tr>
				<td><span>Search for people: </span></td>
				<td><input name="firstName" type="text"></td>
				<td><input name="lastName" type="text"></td>
				<td><input name="address" type="text"></td>
				<td><input autocomplete="off" onkeyup="getCities();" id="cityInput" name="city" type="text">
					<div id="city-suggest" style="display: none;"></div>
				</td>
				<td><select name="country">
						<c:forEach var="country" items="${countries}">
							<option value="${country.id}">${country.description}</option>
						</c:forEach>
				</select></td>
				<td><input type="submit" value="Search"></td>
			</tr>
			<tr>
				<td><a href="./homeAction.do">Go Home</a></td>
			</tr>
		</tbody>
	</form>
</table>
<c:if test="${not empty results}">
	<table>
		<tbody>
			<tr>
				<td colspan="3"><span>Search results:</span></td>
			</tr>
			<c:forEach var="user" items="${results}">
				<tr>
					<td><c:out value="${user.firstName}" /> <c:out
							value="${user.lastName}" /></td>
					<td><a href="./viewProfileAction.do?userId=${user.entityId}">View
							Profile</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</c:if>
<%@include file="/WEB-INF/jsp/footer.jsp"%>