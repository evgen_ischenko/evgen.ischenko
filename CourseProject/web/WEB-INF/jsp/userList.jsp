<%@include file="/WEB-INF/jsp/header.jsp"%>
<table>
	<tbody>
		<tr>
			<td><a href="./homeAction.do">Go to your profile</a></td>
			<td>
				<table>
					<c:forEach var="user" items="${users}">
						<tr>
							<td><c:out value="${user.firstName}" /></td>
							<td><c:out value="${user.lastName}" /></td>
							<td><a href="./viewProfileAction.do?userId=${user.entityId}">View Profile</a></td>
						</tr>
					</c:forEach>
			</td>
		</tr>
</table>
</tr>
</tbody>
</table>
<%@include file="/WEB-INF/jsp/footer.jsp"%>