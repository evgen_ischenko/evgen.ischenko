<%@include file="/WEB-INF/jsp/header.jsp"%>
<table>
<tbody>
	<tr>
		<td colspan="2"><a href="./homeAction.do">Go Back to your
				profile</a></td>
	</tr>
	<tr>
		<td>
			<table>
				<tbody>
					<tr>
						<td colspan="2">Income Messages:</td>
					</tr>
					<c:forEach var="income" items="${loggedUser.incomeMessages}">
						<tr>
							<td class="msgHead"><span>From:
									${income.from.firstName}&nbsp;${income.from.lastName}.</span></td>
							<td class="msgHead"><span>When: ${income.date}</span></td>
						</tr>
						<tr>
							<td class="msgBody" colspan="2">${income.message}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</td>
		<td>
			<table>
				<tbody>
					<tr>
						<td colspan="2">Outcome Messages:</td>
					</tr>
					<c:forEach var="otcome" items="${loggedUser.outcomeMessages}">
						<tr>
							<td class="msgHead"><span>To:
									${otcome.to.firstName}&nbsp;${otcome.to.lastName}.</span></td>
							<td class="msgHead"><span>When: ${otcome.date}</span></td>
						</tr>
						<tr>
							<td class="msgBody" colspan="2">${otcome.message}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</td>
	</tr>
</tbody>
</table>
<%@include file="/WEB-INF/jsp/footer.jsp"%>