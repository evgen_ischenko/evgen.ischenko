<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<form name="likeForm">
		<div class="iconsContainer">
		<c:choose>
			<c:when test="${like.status == 1}">
				<div class="hateContainer" style="float: left;">
					<img src="http://localhost:8080/sn/pic/hateIcon.png"></div>
				<div class="unRockContainer" style="float: left;">
					<img src="http://localhost:8080/sn/pic/unRockIcon.png"></div>
			</c:when>
			<c:when test="${like.status == 2}">
				<div class="unHateContainer" style="float: left;">
					<img src="http://localhost:8080/sn/pic/unHateIcon.png"></div>
				<div class="rockContainer" style="float: left;">
					<img src="http://localhost:8080/sn/pic/rockIcon.png"></div>
			</c:when>
			<c:otherwise>
				<div class="hateContainer" style="float: left;">
					<img src="http://localhost:8080/sn/pic/hateIcon.png"></div>
				<div class="rockContainer" style="float: left;">
					<img src="http://localhost:8080/sn/pic/rockIcon.png"></div>
			</c:otherwise>
		</c:choose>	
		</div>
		<div class="likeCounter" style="float: left; height: 31px; width: 30px; 
			border-color: black; border-width: 1px; border-style: groove;"><span style="width: 28px;; text-align: center; 
			display: inline-block; vertical-align: middle; margin-top: 6px;">${like.count}</span></div>
		<input type="hidden" name="likeStatus" value="${like.status}">
		<input type="hidden" name="likeCount" value="${like.count}">
	</form>
