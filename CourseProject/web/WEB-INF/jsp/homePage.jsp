<%@include file="/WEB-INF/jsp/header.jsp"%>
<table>
	<tbody>
		<tr>
			<td><c:choose>
					<c:when test="${user.avatar == 0}">
						<img class="avatar" src="./pic/anonymous.jpg"
							style="width: 199px; height: 200px;">
					</c:when>
					<c:otherwise>
						<img class="avatar" src="./getImageAction.do?pictureId=${user.avatar}">
					</c:otherwise>
				</c:choose></td>
			<td><table>
					<tbody>
						<tr>
							<td>User Data:</td>
							<td></td>
						</tr>
						<tr>
							<td>First name:</td>
							<td><c:out value="${user.firstName}" /></td>
						</tr>
						<tr>
							<td>Last name:</td>
							<td><c:out value="${user.lastName}" /></td>
						</tr>
					</tbody>
				</table></td>
		</tr>
		<tr>
			<td>
				<table>
					<tbody>
						<c:if test="${loggedUser.entityId != user.entityId}">
							<tr>
								<td><a
									href="#" onclick="showPopup();";>Write
										a message</a></td>
							</tr>
						</c:if>
						<c:if test="${loggedUser.entityId == user.entityId}">
							<tr>
								<td><a href="./viewMessagesAction.do">View Messages</a></td>
							</tr>
						</c:if>
						<tr>
							<td><a href="./enterChatAction.do">Enter Chat</a></td>
						</tr>
						<tr>
							<td><a href="./peopleSearchAction.do?action=load">Search People</a></td>
						</tr>
						<tr>
							<c:if test="${loggedUser.entityId == user.entityId}">
								<td><a href="./editProfileAction.do?action=load">Edit
										Personal Data</a></td>
							</c:if>
						</tr>
						<tr>
							<td><a href="./loadFriendsListAction.do?userId=${user.entityId}">View Friends</a></td>
						</tr>
						<tr>
							<td><c:choose>
									<c:when test="${loggedUser.entityId == user.entityId}">
										<a href="./loadIgnoreListAction.do">View Ignore List</a>
									</c:when>
									<c:otherwise>
										<a href="./addToFriendsAction.do?userId=${user.entityId}">Add
											to friend list</a>
									</c:otherwise>
								</c:choose></td>
						</tr>
						<tr>
							<td><c:if test="${loggedUser.entityId != user.entityId}">
									<a href="./homeAction.do">Go back to your profile</a>
								</c:if></td>
						</tr>
						<tr></tr>
					</tbody>
				</table>
			</td>
			<td><%@include file="/WEB-INF/jsp/userProfile/wall.jsp"%></td>
		</tr>
	</tbody>
</table>
<div class="popup" id="msgContainer" style="visibility: visible; position: fixed; z-index: 1; left: 250px; top: 250px;"
		onsubmit="hidePopup();">
<table>
	<form method="post" action="./writeMessageAction.do?userId=${user.entityId}" id="msgForm" onsubmit="return sendMessage();">
	<tr>
		<td>Write a message below:</td>
		<td><span class="closeButton" onclick="hidePopup();">X</span></td>
	</tr>
	<tr>
		<td colspan="2"><textarea name="message"></textarea></td>
	</tr>
	<tr>
		<td colspan="2">
		<input type="hidden" name="to" value="${user.entityId}">
		<input type="submit">
		</td>
	</tr>
	</form>
</table>
</div>

<%@include file="/WEB-INF/jsp/footer.jsp"%>