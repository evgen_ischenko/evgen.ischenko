<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:forEach var="message" items="${chatMessages}">
	<div>
		<span>[<c:out value="${message.timestamp}" />]&nbsp; <c:out
				value="${message.author.firstName}" />&nbsp; <c:out
				value="${message.author.lastName}" />:&nbsp; <c:out
				value="${message.message}" />
		</span>
	</div>
</c:forEach>