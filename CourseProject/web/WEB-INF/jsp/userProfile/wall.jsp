<script> 
$(document).ready(function(){
  $("#writeLink").click(function(){
    $("#wallForm").slideDown("slow");
  });
});
</script>
<table>
	<tbody>
		<tr>
			<td><c:choose>
					<c:when test="${loggedUser.entityId == user.entityId}">
						<span>Your Wall: </span>
					</c:when>
					<c:otherwise>
						<span>Wall of ${user.firstName}&nbsp;${user.lastName}</span>
					</c:otherwise>
				</c:choose></td>
		</tr>
		<c:set var="i" value="${0}"/>
		<c:forEach items="${user.wall.posts}" var="post">
			<tr>
				<td><div id="post_${i}">
					<%@include file="/WEB-INF/jsp/userProfile/post.jsp"%>
						<div class="likeContainer">
						</div>
					</div>
				</td>
			</tr>
			<c:set var="i" value="${i+1}" />
		</c:forEach>
		<tr>
			<td>
			<div id="wallForm" style="display: none;">
				<%@include
					file="/WEB-INF/jsp/wallForm.jsp"%>
					</div></td>
		</tr>
		<tr>
			<td><a id="writeLink" href="#">Write something</a></td>
		</tr>
	</tbody>
</table>