<table>
	<tbody>
		<tr>
			<td><span><c:out value="${post.owner.firstName}" /></span></td>
			<td><span><c:out value="${post.text}" /></span></td>
		</tr>
		<tr>
			<td></td>
			<td>
					<c:if test="${!empty post.picture && post.picture.entityId != 0}">
						<img src="./getImageAction.do?pictureId=${post.picture.entityId}"/>
					</c:if>
					<c:if test="${post.video != null}">video goes here...</c:if>
			</td>
		</tr>
	</tbody>
</table>