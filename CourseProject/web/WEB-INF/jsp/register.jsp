<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>socialNetwork</title>
<link rel="stylesheet" type="text/css" href="./css/common.css">
<link rel="stylesheet" type="text/css"
	href="./css/jquery-ui-1.10.2.custom.css">
<script src="./js/jquery-1.9.0.js"></script>
<script src="./js/jquery-ui-1.10.2.custom.js"></script>
<script src="./js/validation.js"></script>
<script src="./js/city.js"></script>
<script type="text/javascript">
	jQuery.browser = {};
	jQuery.browser.mozilla = /mozilla/.test(navigator.userAgent.toLowerCase())
			&& !/webkit/.test(navigator.userAgent.toLowerCase());
	jQuery.browser.webkit = /webkit/.test(navigator.userAgent.toLowerCase());
	jQuery.browser.opera = /opera/.test(navigator.userAgent.toLowerCase());
	jQuery.browser.msie = /msie/.test(navigator.userAgent.toLowerCase());

	$(function() {
		if (!jQuery.browser.webkit) {
			$("#datepicker").datepicker({
				dateFormat : 'yy-mm-dd'
			});
		} else {
			//			var months = {};
			//			months["Jan"] = '01';
			//			months["Feb"] = '02';
			//			months["Mar"] = '03';
			//			months["Apr"] = '04';
			//			months["May"] = '05';
			//			months["Jun"] = '06';
			//			months["Jul"] = '07';
			//			months["Aug"] = '08';
			//			months["Sep"] = '09';
			//			months["Oct"] = '10';
			//			months["Nov"] = '11';
			//			months["Dec"] = '12';
			var date = $("#datepicker").attr("value").toString();
			var month = date.substr(5, 2)
			var year = date.substr(0, 4);
			var day = date.substr(8, 2);
			$("#datepicker").attr("value", year + "-" + month + "-" + day);
		}
	});
</script>
</head>
<body>
	<form action="./${action}?action=done" name="registerForm"
		method="post" onsubmit="return validateForm()">
		<fieldset>
			<table>
				<tbody>
					<tr>
						<td><label for="firstName">First Name*: </label></td>
						<td><input name="firstName" type="text"
							onchange="validateFirstName()" value="${loggedUser.firstName}"></td>
						<td><span class="errorLabel" id="fName"></span></td>
					</tr>
					<tr>
						<td><label for="lastName">Last Name*: </label></td>
						<td><input name="lastName" type="text"
							onchange="validateLastName()" value="${loggedUser.lastName}"></td>
						<td><span class="errorLabel" id="lName"></span></td>
					</tr>
					<tr>
						<td><label for="userName">Login*: </label></td>
						<td><input name="userName" type="text"
							onchange="validateUserName()" value="${loggedUser.userName}"></td>
						<td><span class="errorLabel" id="uName"></span></td>
					</tr>
					<tr>
						<td><label for="password">Password*: </label></td>
						<td><input name="password" type="password"
							onchange="validatePassword()" value="${loggedUser.password}"></td>
						<td><span class="errorLabel" id="pass1"></span></td>
					</tr>
					<tr>
						<td><label for="retypePassword">Repeat Password*: </label></td>
						<td><input name="retypePassword" type="password"
							onchange="validatePassword2()" value="${loggedUser.password}"></td>
						<td><span class="errorLabel" id="pass2"></span></td>
					</tr>
					<tr>
						<td><label for="email">Email Address*: </label></td>
						<td><input name="email" type="text"
							onchange="validateEmail()" value="${loggedUser.email}"></td>
						<td><span class="errorLabel" id="mail"></span></td>
					</tr>
					<tr>
						<td><label for="country">Select country: </label>
						<td><select
							style="width: 153px; height: 22px; margin-left: 2px;"
							name="country">
								<c:if test="${!empty loggedUser}">
									<option value="${loggedUser.country.id}" selected="selected">${loggedUser.country.description}
									
								</c:if>
								</option>
								<c:forEach items="${countries}" var="country">
									<c:if test="${loggedUser.country.id != country.id}">
										<option value="${country.id}">${country.description}</option>
									</c:if>
								</c:forEach>
						</select></td>
						<td><span class="errorLabel" id="cntry"></span></td>
					</tr>
					<tr>
						<td><label for="city">Select city: </label></td>
						<td><input autocomplete="off" onkeyup="getCities();" id="cityInput" name="city"
							type="text" value="${loggedUser.city}">
							<div id="city-suggest" style="display: none;"></div></td>
						<td></td>
					</tr>
					<tr>
						<td><label for="address">Address: </label></td>
						<td><input name="address" type="text"
							value="${loggedUser.address}"></td>
						<td><span class="errorLabel" id="address"></span></td>
					</tr>
					<tr>
						<td><label for="birthDate">Birth Date: </label></td>
						<td><input id="datepicker"
							style="width: 148px; margin-left: 2px;" name="birthDate"
							type="date" value="${loggedUser.dateOfBirth}"></td>
						<td><span class="errorLabel" id="birth"></span></td>
					</tr>
				</tbody>
			</table>
		</fieldset>
		<input type="submit"><span style="color: red;">&nbsp;*
			Denotes required fields.</span>
	</form>
	<c:choose>
		<c:when test="${action == 'registerAction.do'}">
			<a href="./loginAction.do">Go back to login page</a>
		</c:when>
		<c:otherwise>
			<a href="./homeAction.do">Go back to your profile</a>
		</c:otherwise>
	</c:choose>
</body>
</html>