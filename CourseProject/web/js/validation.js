function validateForm() {
    if (validateUserName() && validateLastName() && validateFirstName()
	    && validatePassword() && validatePassword2() && validateEmail()) {
	return true;
    } else {
	alert("Something went wrong. Recheck entered data.");
	return false;
    }
}

function validateUserName() {
    var element = document.forms["registerForm"]["userName"].value;
    if (null == element || element == "") {
	$("#uName").text("User Name can not be empty.");
	$("#uName").css("visibility", "visible");
	return false;
    } else {
	$("#uName").css("visibility", "hidden");
	return true;
    }
}

function validateLastName() {
    var element = document.forms["registerForm"]["lastName"].value;
    if (null == element || element == "") {
	$("#lName").text("Last Name can not be empty.");
	$("#lName").css("visibility", "visible");
	return false;
    } else {
	$("#lName").css("visibility", "hidden");
	return true;
    }
}

function validateFirstName() {
    var element = document.forms["registerForm"]["firstName"].value;
    if (null == element || element == "") {
	$("#fName").text("First Name can not be empty.");
	$("#fName").css("visibility", "visible");
	return false;
    } else {
	$("#fName").css("visibility", "hidden");
	return true;
    }
}

function validatePassword() {
    var element = document.forms["registerForm"]["password"].value;
    if (null == element || element == "") {
	$("#pass1").text("Password can not be empty.");
	$("#pass1").css("visibility", "visible");
	return false;
    } else if (element.length < 6 || element.length > 12) {
	$("#pass1").text("Password sohuld be 6 - 12 characters long.");
	$("#pass1").css("visibility", "visible");
	return false;
    } else {
	$("#pass1").css("visibility", "hidden");
	return true;
    }
}

function validatePassword2() {
    var element = document.forms["registerForm"]["retypePassword"].value;
    if (element != $("[name='password']").val()) {
	$("#pass2").text("Password is wrong.");
	$("#pass2").css("visibility", "visible");
	return false;
    } else {
	$("#pass2").css("visibility", "hidden");
	return true;
    }
}

function validateEmail() {
    var element = $("[name='email']").val();
    var regex = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
    if (element.match(regex) == null) {
	$("#mail").text("Email is wrong.");
	$("#mail").css("visibility", "visible");
	return false;
    } else {
	$("#mail").css("visibility", "hidden");
	return true;
    }
}
