$(document).click(function(event) {
    if (!$(event.target).hasClass('#city-suggest')) {
    	closeAutosuggest();
    }
});

function putCitiesToSuggest(object) {
	if (object != null) {
		$("#city-suggest").html("");
		var i = 1;
		while (object[i] != null && i < 10) {
			$("#city-suggest").css("display", "block");
			$("#city-suggest").append("<div id=\"city-suggest-item" + i + "\">" + object[i] + "</div>");
			$("#city-suggest-item" + i).attr("onclick", "selectCity(\"" + object[i] + "\")");
			$("#city-suggest-item" + i).addClass("city-suggest-item");
			i = i + 1;
		}
	}
}

function getCities() {
	var city = document.forms[0]["cityInput"].value;
	$.ajax("./getCitiesForSuggestAction.do?city=" + city).done(function(json){
		if (json != "") {
			obj = JSON.parse(json);
			putCitiesToSuggest(obj);
		} else {
			closeAutosuggest();
		}
	});
}

function selectCity(city) {
	document.forms[0]["cityInput"].value = city;
	closeAutosuggest();
}

function closeAutosuggest() {
	$("#city-suggest").css("display", "none");
	$("#city-suggest").html("");
}