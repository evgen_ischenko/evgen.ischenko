function showPopup() {
	$("#msgContainer").slideDown('slow');
};

function hidePopup() {
	$("#msgContainer").slideUp("slow");
};

function sendMessage() {
	$.ajax({
		type: "POST",
		url: $("#msgForm").attr("action"),
		data: $("#msgForm").serialize()
	});
	return false;
};

