$(document).ready(function() {
	
	$(".likeContainer").each(function() {
		var ajaxURL = "http://localhost:8080/sn/getLikeFormAction.do";
		prepareParams($(this), ajaxURL);
	});

});


$(document).on('click', ".hateContainer", function() {
	var elem = findParentContainer($(this));
	doLike(2, elem);
});
	
$(document).on('click', ".rockContainer", function() {
	var elem = findParentContainer($(this));
	doLike(1, elem);
});

$(document).on('click', ".unHateContainer", function() {
	var elem = findParentContainer($(this));
	doLike(0, elem);
});

$(document).on('click', ".unRockContainer", function() {
	var elem = findParentContainer($(this));
	doLike(0, elem);
});


function doLike(status, elem) {
	var ajaxURL = "http://localhost:8080/sn/doLikeAction.do";
	prepareParams(elem, ajaxURL, status);
}

function prepareParams(elem, ajaxURL, status) {
	ajaxURL += "?url=" + encodeURIComponent($(location).attr("href"));
	ajaxURL += "&likedContentId=" + elem.parent().attr("id");
//	ajaxURL += "&content=" + $(this).parent().html();
	if (status != null) {
		ajaxURL += "&status=" + status;
	}
	sendAjax(elem, ajaxURL);
} 

function findParentContainer(elem) {
	return elem.closest(".likeContainer");
}

function sendAjax(elem, ajaxURL) {
	$.ajax(ajaxURL).done(function(html) {
		elem.html(html);
	});
}	
