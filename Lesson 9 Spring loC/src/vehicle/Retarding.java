package vehicle;

public interface Retarding {

	public void applyBrakes(int brakingForce, int time);
	
}
