package vehicle;

public class ControlPanel {

	private Engine engine;
	private RearWheel wheel;
	private Accelerator accelerator;
	private BrakePedal brakePedal;
	private GasTank gasTank;
	private HandBrake handBrake;
	private Horn horn;
	private SteeringWheel steeringWheel;
	private double speed;
	

	public double getSpeed() {
		speed = (wheel.CdR/1000) * (engine.getRpm()*60);
		return speed;
	}
	
	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public RearWheel getWheel() {
		return wheel;
	}

	public void setWheel(RearWheel wheel) {
		this.wheel = wheel;
	}

	public Accelerator getAccelerator() {
		return accelerator;
	}
	
	public void setAccelerator(Accelerator accelerator) {
		this.accelerator = accelerator;
	}
	
	public BrakePedal getBrakePedal() {
		return brakePedal;
	}
	
	public void setBrakePedal(BrakePedal brakePedal) {
		this.brakePedal = brakePedal;
	}
	
	public GasTank getGasTank() {
		return gasTank;
	}
	
	public void setGasTank(GasTank gasTank) {
		this.gasTank = gasTank;
	}
	
	public HandBrake getHandBrake() {
		return handBrake;
	}
	
	public void setHandBrake(HandBrake handBrake) {
		this.handBrake = handBrake;
	}
	
	public Horn getHorn() {
		return horn;
	}
	
	public void setHorn(Horn horn) {
		this.horn = horn;
	}
	
	public SteeringWheel getSteeringWheel() {
		return steeringWheel;
	}
	
	public void setSteeringWheel(SteeringWheel steeringWheel) {
		this.steeringWheel = steeringWheel;
	}

	
}
