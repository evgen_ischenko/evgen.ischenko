package vehicle;

public class GasTank {
	
	private int gasLevel = 100;

	public void fillGasTank(int amount) {
		gasLevel = gasLevel + amount;
	}
	
	public void useGas(int amount) {
		gasLevel = gasLevel - amount;
	}
	
	public int getGasLevel() {
		return gasLevel;
	}

	public void setGasLevel(int gasLevel) {
		this.gasLevel = gasLevel;
	}
	
}
