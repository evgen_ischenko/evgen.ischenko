package vehicle;

public class Engine implements Runnable {

	private Integer rpm = 0;
	private ForwardWheel leftForwardWheel;
	private ForwardWheel rightForwardWheel;
	private RearWheel leftRearWheel;
	private RearWheel rightRearWheel;
	private GasTank gasTank;
	private Thread workingEngine;
	private boolean isStarted = false;

	public void startEngine() {
		if (isStarted == false) {
			isStarted = true;
			workingEngine = new Thread(this);
			workingEngine.start();
		}
	}
	
	public void stopEngine() {
		isStarted = false;
		if (!workingEngine.isInterrupted()) {
			workingEngine.interrupt();
		}
	}

	private void work() {
		while (true) {
			if (gasTank.getGasLevel() == 0) {
				stopEngine();
				System.out.println("Gas tank is empty, engine stopped");
				break;
			}
			gasTank.useGas(1);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void increaseRpm(int time) {
		for (int i = 0; i < time; i++) {
			if (gasTank.getGasLevel() > 0) {
				if (i < 10) {
					rpm = rpm + 100;
					leftRearWheel.accelerate(95);
					rightRearWheel.accelerate(95);
					leftForwardWheel.accelerate(95);
					rightForwardWheel.accelerate(95);
					gasTank.useGas(3);
				} else if (i < 15) {
					rpm = rpm + 75;
					leftRearWheel.accelerate(40);
					rightRearWheel.accelerate(40);
					leftForwardWheel.accelerate(40);
					rightForwardWheel.accelerate(40);
					gasTank.useGas(2);
				} else if (i < 25) {
					rpm = rpm + 40;
					leftRearWheel.accelerate(15);
					rightRearWheel.accelerate(15);
					leftForwardWheel.accelerate(15);
					rightForwardWheel.accelerate(15);
					gasTank.useGas(1);
				}
			}
		}
	}

	public GasTank getGasTank() {
		return gasTank;
	}

	public void setGasTank(GasTank gasTank) {
		this.gasTank = gasTank;
	}

	public Integer getRpm() {
		return rpm;
	}

	public void setRpm(Integer rPM) {
		this.rpm = rPM;
	}

	public RearWheel getLeftRearWheel() {
		return leftRearWheel;
	}

	public void setLeftRearWheel(RearWheel leftRearWheel) {
		this.leftRearWheel = leftRearWheel;
	}

	public ForwardWheel getLeftForwardWheel() {
		return leftForwardWheel;
	}

	public void setLeftForwardWheel(ForwardWheel leftForwardWheel) {
		this.leftForwardWheel = leftForwardWheel;
	}

	public ForwardWheel getRightForwardWheel() {
		return rightForwardWheel;
	}

	public void setRightForwardWheel(ForwardWheel rightForwardWheel) {
		this.rightForwardWheel = rightForwardWheel;
	}

	public RearWheel getRightRearWheel() {
		return rightRearWheel;
	}

	public void setRightRearWheel(RearWheel rightRearWheel) {
		this.rightRearWheel = rightRearWheel;
	}

	@Override
	public void run() {
		work();
	}

}
