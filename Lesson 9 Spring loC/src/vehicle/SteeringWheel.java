package vehicle;

public class SteeringWheel {

	private Integer steeringAngle = 0;
	private ForwardWheel leftForwardWheel;
	private ForwardWheel rightForwardWheel;
	
	public void turnLeft(int angle) {
		steeringAngle = steeringAngle - angle;
		leftForwardWheel.turnLeft(angle/2);
		rightForwardWheel.turnLeft(angle/2);
	}
	
	public void turnRight(int angle) {
		steeringAngle = steeringAngle + angle;
		leftForwardWheel.turnRight(angle/2);
		rightForwardWheel.turnRight(angle/2);
	}
	
	public ForwardWheel getLeftForwardWheel() {
		return leftForwardWheel;
	}

	public void setLeftForwardWheel(ForwardWheel leftForwardWheel) {
		this.leftForwardWheel = leftForwardWheel;
	}

	public ForwardWheel getRightForwardWheel() {
		return rightForwardWheel;
	}

	public void setRightForwardWheel(ForwardWheel rightForwardWheel) {
		this.rightForwardWheel = rightForwardWheel;
	}

	public Integer getSteeringAngle() {
		return steeringAngle;
	}

	public void setSteeringAngle(Integer steeringAngle) {
		this.steeringAngle = steeringAngle;
	}
	
}
