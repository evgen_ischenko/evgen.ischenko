package vehicle;

public class ForwardWheel extends Wheel implements Steared, Retarding {

	private int wheelAngle = 0;
	private int rpm = 0;
	
	@Override
	public void turnLeft(int angle) {
		wheelAngle = wheelAngle - angle;
	}

	@Override
	public void turnRight(int angle) {
		wheelAngle = wheelAngle + angle;
	}
	
	@Override
	public void applyBrakes(int brakingForce, int time) {
		// TODO Auto-generated method stub
	}
	
	public int getWheelAngle() {
		return wheelAngle;
	}
	
	public void setWheelAngle(int wheelAngle) {
		this.wheelAngle = wheelAngle;
	}
	
	public void accelerate(int incRpm) {
		rpm = rpm + incRpm;
	}
	
}
