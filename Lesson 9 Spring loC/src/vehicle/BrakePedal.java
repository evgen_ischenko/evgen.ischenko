package vehicle;

public class BrakePedal {

	private ForwardWheel leftForwardWheel;
	private ForwardWheel rightForwardWheel;
	private RearWheel leftRearWheel;
	private RearWheel rightRearWheel;
	
	
	public ForwardWheel getLeftForwardWheel() {
		return leftForwardWheel;
	}
	public void setLeftForwardWheel(ForwardWheel leftForwardWheel) {
		this.leftForwardWheel = leftForwardWheel;
	}
	public ForwardWheel getRightForwardWheel() {
		return rightForwardWheel;
	}
	public void setRightForwardWheel(ForwardWheel righForwardWheel) {
		this.rightForwardWheel = righForwardWheel;
	}
	public RearWheel getLeftRearWheel() {
		return leftRearWheel;
	}
	public void setLeftRearWheel(RearWheel leftRearWheel) {
		this.leftRearWheel = leftRearWheel;
	}
	public RearWheel getRightRearWheel() {
		return rightRearWheel;
	}
	public void setRightRearWheel(RearWheel rightRearWheel) {
		this.rightRearWheel = rightRearWheel;
	}
	
}
