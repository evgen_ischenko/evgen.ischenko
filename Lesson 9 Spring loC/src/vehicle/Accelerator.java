package vehicle;

public class Accelerator {

	private Engine engine;
	
	public void pushAccelerator(int time) {
		engine.increaseRpm(time);
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}
	
}
