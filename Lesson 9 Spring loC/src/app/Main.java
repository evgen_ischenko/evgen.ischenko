package app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import vehicle.ControlPanel;
import vehicle.Vehicle;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ApplicationContext context = 
				new ClassPathXmlApplicationContext("Beans.xml");
		
		Vehicle vehicle = (Vehicle) context.getBean("vehicle");

		System.out.println("Steering angle is: " + vehicle.getControlPanel().getSteeringWheel().getSteeringAngle());
		
		vehicle.getControlPanel().getSteeringWheel().turnLeft(30);
		
		System.out.println("Steering angle is: " + vehicle.getControlPanel().getSteeringWheel().getSteeringAngle());
		System.out.println("Wheel angle is: " + vehicle.getControlPanel().getSteeringWheel().getLeftForwardWheel().getWheelAngle());
		
		System.out.println("Engine rPM is: " + vehicle.getControlPanel().getEngine().getRpm());

		vehicle.getControlPanel().getAccelerator().pushAccelerator(20);
		
		System.out.println("Engine rPM is: " + vehicle.getControlPanel().getEngine().getRpm());
		
		System.out.println("Speed is " + vehicle.getControlPanel().getSpeed() + " km/h");
		
		vehicle.getControlPanel().getHorn().pushHorn();
		
		ControlPanel c = vehicle.getControlPanel();
		
		c.getAccelerator().getEngine().startEngine();
		
	}

}
